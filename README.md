# DIETERpy_reduced

This is DIETERpy_reduced, a reduced version of DIETERpy (https://doi.org/10.1016/j.softx.2021.100784).

To use this model:

1. install all packages in requirements.txt to a dedicated python environment
2. configure the model in configure.xlsx.
3. run runopt.py
4. use result_analysis.py to analyze results and generate figures