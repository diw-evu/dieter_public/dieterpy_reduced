*_______________________________________________________________________________

*  This is a reduced version of DIETERpy (Dispatch and Investment Evaluation Tool with Endogenous Renewables in Python).
*  It is used for the paper "Renewable Energy Targets and Unintended Storage Cycling: Implications for Energy Modeling"
*  Coded by Martin Kittel 
*  Version: 1.0
*  Date of this version: June 2021
*  For general documentation of DIETER see Zerrahn & Schill (2018): https://doi.org/10.1016/j.rser.2016.11.098
*  For general documentation of DIETERpy see Gaete-Morales et al. (2021): https://arxiv.org/abs/2010.00883
*  This tool can do more than what is used in the paper. Any feedback is welcome.

*-------------------------------------------------------------------------------

*  Lizenz notwendig?

*  This code is licensed under an MIT license.
*  Copyright (c) 2021 Martin Kittel <mkittel@diw.de>
*  SPDX-License-Identifier: MIT
*  Permission is hereby granted, free of charge, to any person obtaining a
*  copy of this software and associated documentation files (the "Software"),
*  to deal in the Software without restriction, including without limitation
*  the rights to use, copy, modify, merge, publish, distribute, sublicense,
*  and/or sell copies of the Software, and to permit persons to whom the
*  Software is furnished to do so, subject to the following conditions:

*  The above copyright notice and this permission notice shall be included in
*  all copies or substantial portions of the Software.

*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
*  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
*  IN THE SOFTWARE.

*________________________________________________________________________________


* ______________ OPTIONS, FIXINGS, REPORT PREPARATION __________________________

options

* The optcr setting is a tolerance for the relative optimality gap in a MIP or other discrete model.
*optcr = 0.00
optcr = 1e-3
optca = 10
* The reslim specifies the maximum time in seconds that a solver may run before it terminates.
reslim = 10000000
*solvers
lp = cplex
* dispwidth may be used to display longer names in full, such as, label names that are headers of columns in tables. Cut off after X characters
dispwidth = 15
limrow = 0
limcol = 0
* solprint remove solution listings following solves.
*solprint = off
* sysout dont suppress additional solver generated output.
sysout = on
;

* ______________ GLOBAL OPTIONS _________________________________________________
* Set global options in the file config.xlsx w/o any quotation marks. Do not
* change assignements below!

* ------------- BASE YEAR ------------------------------------------------------
* Select year by setting the global option 'base_year' between '2012' and '2016':
$setglobal base_year "%py_base_year%"

* ------------- TIME HORIZON ---------------------------------------------------
* Set end hour by setting the global option 'end_hour' between 'h1' and 'h8760':
$setglobal end_hour "%py_end_hour%"

* ------------- STORAGE --------------------------------------------------------
* Select if storage may only charge renewables or also conventional electricity
* by setting the global option to 'yes':

* Storage only for renewables?
$setglobal sto_res_only "%py_sto_res_only%"

* Storage also for conventional energy?
$setglobal sto_anything "%py_sto_anything%"

* ------------- RES SHARE CONSTRAINT FORMULATION -------------------------------
* Select a RES share constraint implementation by setting the global option to
* 'yes':

* RES share constraint (1a): G_RES- and demand-based, storage losses completely
* covered by conventional generation technologies
$setglobal rescon_1a "%py_rescon_1a%"

* RES share constraint (1b): G_RES- and demand-based, storage losses covered by
* renewable generation technologies proportional to phi_min_res
$setglobal rescon_1b "%py_rescon_1b%"

* RES share constraint (1c): G_RES-and demand-based, storage losses completely
* covered by renewable generation technologies
$setglobal rescon_1c "%py_rescon_1c%"

* RES share constraint (2a): G_RES- and total-generation-based, storage losses
* completely covered by conventional generation technologies
$setglobal rescon_2a "%py_rescon_2a%"

* RES share constraint (2b): G_RES- and total-generation-based, storage losses
* covered by RES proportional to phi_min_res)
$setglobal rescon_2b "%py_rescon_2b%"

* RES share constraint (2c): G_RES- and total-generation-based, storage losses
* completely covered by renewable generation technologies
$setglobal rescon_2c "%py_rescon_2c%"

* RES share constraints (3a): G_CON- and demand-based, storage losses
* completely covered by conventional generation technologies)
$setglobal rescon_3a "%py_rescon_3a%"

* RES share constraint (3b): G_CON- and demand-based, storage losses covered
* by conventional generation technologies proportional to (1-phi_min_res)
$setglobal rescon_3b "%py_rescon_3b%"

* RES share constraint (3c): G_CON- and demand-based, storage losses
* completely covered by renewable generation technologies
$setglobal rescon_3c "%py_rescon_3c%"

* RES share constraint (4a): G_CON- and total-generation-based, storage losses
* covered by conventional generation technologies
$setglobal rescon_4a "%py_rescon_4a%"

* RES share constraint (4b): G_CON- and total-generation-based, storage losses
* covered by conventional generation technologies proportional to (1-phi_min_res)
$setglobal rescon_4b "%py_rescon_4b%"

* RES share constraint (4c): G_CON- and total-generation-based, storage losses
* completely covered by renewable generation technologies
$setglobal rescon_4c "%py_rescon_4c%"

* CO2 constraint (5a): CO2 budget with implicit CO2 price
$setglobal co2con_5a "%py_co2con_5a%"

* CO2 constraint (5b): explicit CO2 price only
$setglobal co2con_5b "%py_co2con_5b%"

* ------------- P2X ------------------------------------------------------------
* Select if additional flexible power-to-x demand should be considered by setting the global option to 'yes'
$setglobal p2x "%py_p2x%"
* Auxiliary string (do not change):
$if "%p2x%" == "" $setglobal not_p2x "*"
$if "%p2x%" == "*" $setglobal not_p2x ""

* ------------- CPLEX OPTIONS --------------------------------------------------
* Select only when barrier is set in cplex by setting the global option to 'yes'
$setglobal thread %py_threads%
$setglobal no_crossover %py_nocross%

* ------------- DISPLAY GLOBAL OPTIONS -----------------------------------------
display "%base_year%","%end_hour%","%sto_res_only%","%sto_anything%",
"%rescon_1a%","%rescon_1b%","%rescon_1c%",
"%rescon_2a%","%rescon_2b%","%rescon_2c%",
"%rescon_3a%","%rescon_3b%","%rescon_3c%",
"%rescon_4a%","%rescon_4b%","%rescon_4c%",
"%co2con_5a%","%co2con_5b%","%p2x%"; 

display "%thread%", "%no_crossover%";

* ------------- Sanity checks --------------------------------------------------
$if "%sto_res_only%" == "%sto_anything%" $abort Please select either sto_res_only or sto_anything!

*________________________________________________________________________________

* ______________ DECLARATIONS ___________________________________________________

Sets
h                Hours
ct               Dispatchable Technologies
res              Renewable technologies
sto              Storage technolgies
p2x              Power-to-x technologies                 /p2x/
year             Base years

* for data upload
headers_con
headers_res
headers_sto
headers_sys
;

*-------------------------------------------------------------------------------

Parameters
phi_sto_ini(sto)         Level of storage in first and last period of the analysis [0 1]
eta_sto_in(sto)          Efficiency: storage in [0 1]
eta_sto_out(sto)         Efficiency: storage out [0 1]
phi_min_res              Minimum share of renewable electricity in net consumption [%]
phi_max_curt             Maximum share of renewable electricity curtailed over the year [%]
d(h)                     Electricity demand [MWh]
phi_res(res,h)           Hourly capacity factor renewable energy [0 1]
c_i_sto_e(sto)           Cost: investment into storage energy [� per MWh]
c_i_sto_p(sto)           Cost: investment into storage power [� per MW]
c_i_res(res)             Cost: investment into renewable capacity [� per MW]
c_i_con(ct)              Cost: investment into conventional capacity [� per MW]
c_var_con(ct)            Cost: variable generation costs conventional capacity [EUR per MWh]
c_var_sto(sto)           Cost: variable generation costs storage [� per MWh]
c_var_res(res)           Cost: variable generation costs renewable [� per MWh]
c_var_cu(res)            Cost: variable generation costs renewable curtailment [� per MWh]
co2_budget               Cap for total CO2 emissions [tCO2_e]
co2_price                Cost: price per ton CO2 emitted [EUR per tCO2_e]
emission_factor(ct)      Specific emission factor [tCO2_e per MWh_el]
c_carbon_con(ct)         Cost: variable cost for emissions [EUR per MWh_el]
c_var_infes              Cost: penalty cost for slack generation (VOLL) [� per MWh]
p2x_power(p2x)           Power-to-x intake power [MW]
p2x_flh(p2x)             Power-to-x full-load hours [h per year]

* for data upload
d_upload(h,year)                 Electricity demand - upload parameter
phi_res_upload(h,res,year)       Hourly capacity factor renewable energy - upload parameter
tech_con_upload(ct,headers_con)  Conventional technology data - upload parameter
tech_res_upload(res,headers_res) Renewable technology data - upload parameter
tech_sto_upload(sto,headers_sto) Storage technology data - upload parameter
sys_upload(headers_sys)          System data - upload parameter
;
*-------------------------------------------------------------------------------

Variables
Z                        Objective
;

Positive variables
G_CON(ct,h)              Generation of conventional electricity [MWh]
G_RES(res,h)             Generation of renewable energy [MWh]
CU(res,h)                Curtailment of renewable energy [MWh]
N_RES(res)               Capacities: renewable energy  [MW]
N_CON(ct)                Capacities: conventional energy  [MW]
N_STO_E(sto)             Capacities: storage energy [MWh]
N_STO_P_IN(sto)          Capacities: storage charging power [MW]
N_STO_P_OUT(sto)         Capacities: storage discharging power [MW]
STO_L(sto,h)             Storage level [MWh]
STO_IN(sto,h)            Storage intake [MW]
STO_OUT(sto,h)           Storage generation [MW]
P2X_IN(p2x,h)            Power-to-x intake [MWh]
G_INFEAS(h)               Unspecified generator: slack variable for missing generation [MWh]
;

*-------------------------------------------------------------------------------

Scalar
ms 'model status'
ss 'solve status';

*_______________________________________________________________________________

* ______________ DATA UPLOAD ___________________________________________________

$GDXin "gdxin/data_input.gdx"
$load ct headers_con tech_con_upload
$load res headers_res tech_res_upload
$load sto headers_sto tech_sto_upload
$load headers_sys sys_upload
;

$GDXin "gdxin/time_series.gdx"
$load h year d_upload phi_res_upload
;



*_______________________________________________________________________________

* ______________ PARAMATRIZATION _______________________________________________

* Initialize base year
phi_res(res,h) = phi_res_upload(h,res,%base_year%) ;
d(h) = d_upload(h,%base_year%) ;

* Initialize share parameters
phi_min_res = sys_upload('phi_min_res') ;
phi_max_curt = sys_upload('phi_max_curt') ;

* initialize emission parameter
co2_budget = sys_upload('CO2_budget');
co2_price = sys_upload('CO2_price');
emission_factor(ct) = tech_con_upload(ct,'carbon_content')/tech_con_upload(ct,'eta_con');


* Initialize cost parameters
* Values assumed for 2030 (2050)
* Source:  Schill, Wolf-Peter, & Zerrahn, Alexander. (2020, July 8). Reduced version of the model DIETER (Version 1.1). Joule.
*          Zenodo. http://doi.org/10.5281/zenodo.3935702

c_i_con(ct) = tech_con_upload(ct,'oc')*( tech_con_upload(ct,'interest_rate') * (1+tech_con_upload(ct,'interest_rate'))**(tech_con_upload(ct,'lifetime')) )
         / ( (1+tech_con_upload(ct,'interest_rate'))**(tech_con_upload(ct,'lifetime'))-1 )
         + tech_con_upload(ct,'fixed_costs') ;

c_i_res(res) = tech_res_upload(res,'oc')*( tech_res_upload(res,'interest_rate') * (1+tech_res_upload(res,'interest_rate'))**(tech_res_upload(res,'lifetime')) )
         / ( (1+tech_res_upload(res,'interest_rate'))**(tech_res_upload(res,'lifetime'))-1 )
         + tech_res_upload(res,'fixed_costs') ;

c_i_sto_e(sto) = tech_sto_upload(sto,'oc_energy')*( tech_sto_upload(sto,'interest_rate') * (1+tech_sto_upload(sto,'interest_rate'))**(tech_sto_upload(sto,'lifetime')) )
         / ( (1+tech_sto_upload(sto,'interest_rate'))**(tech_sto_upload(sto,'lifetime'))-1 ) ;

c_i_sto_p(sto) = tech_sto_upload(sto,'oc_power')*( tech_sto_upload(sto,'interest_rate') * (1+tech_sto_upload(sto,'interest_rate'))**(tech_sto_upload(sto,'lifetime')) )
         / ( (1+tech_sto_upload(sto,'interest_rate'))**(tech_sto_upload(sto,'lifetime'))-1 ) ;

c_carbon_con(ct) = emission_factor(ct) * co2_price;

c_var_con(ct) = tech_con_upload(ct,'fuel_costs')/tech_con_upload(ct,'eta_con')
* if explicit CO2 pricing
%co2con_5b%$ontext
            + c_carbon_con(ct)
$ontext
$offtext
;

c_var_res(res) = tech_res_upload(res,'variable_costs');
c_var_cu(res) = tech_res_upload(res,'curtailment_costs');

c_var_sto(sto) = tech_sto_upload(sto,'vc') ;

eta_sto_in(sto) = sqrt(tech_sto_upload(sto,'eta_roundtrip')) ;
eta_sto_out(sto) = sqrt(tech_sto_upload(sto,'eta_roundtrip')) ;
phi_sto_ini(sto) = tech_sto_upload(sto,'phi_sto_ini') ;

c_var_infes = 50000;

* Initialize P2X parameter
p2x_power(p2x) = 0;
p2x_flh(p2x) = 0;

*_______________________________________________________________________________

* ______________ MODEL _________________________________________________________

Equations
objective                Objective function
energy_balance           Energy balance (market clearing)
renewable_generation     Use of renewable energy generation
minRES                   Minimum share of renewables or maximum share of conventionals
co2Budget                CO2 Budget Cap
maximum_curtailment      Constraint on maximum share of renewables curtailment
maximum_loss             Constraint on maximum share of renewable energy loss

maximum_generation_con   Capacity constraint - conventional generation
stolev_start_end         Storage: storage level in the first and last period
stolev                   Storage: storage level dynamics
stolev_max               Storage: capacity constraint on maximum energy
maxin_power              Storage: capacity constraint on maximum power - storing in
maxout_power             Storage: capacity constraint on maximum power - storing out

maxin_p2x                Power-to-x: constraint on maximum power - energy intake
flh_p2x                  Power-to-x: full-load hours
;

*-------------------------------------------------------------------------------
** OBJECTIVES

objective..
         Z =E= sum( sto , c_i_sto_e(sto) * N_STO_E(sto) + c_i_sto_p(sto) * N_STO_P_IN(sto) + c_i_sto_p(sto) * N_STO_P_OUT(sto) )
         + sum( res , c_i_res(res) * N_RES(res) )
         + sum( (res,h) , c_var_res(res) * G_RES(res,h) )
         + sum( (res,h) , c_var_cu(res) * CU(res,h) )
         + sum( ct , c_i_con(ct) * N_CON(ct) )
         + sum( (ct,h) , c_var_con(ct) * G_CON(ct,h) )
* if explicit CO2 pricing
%co2con_5b%$ontext
         + sum( (ct,h) , c_carbon_con(ct) * G_CON(ct,h) ) 
$ontext
$offtext
         + sum( (sto,h) , c_var_sto(sto) * (STO_IN(sto,h) + STO_OUT(sto,h)) )
         + sum( h, c_var_infes * G_INFEAS(h) )
;

*-------------------------------------------------------------------------------
** ENERGY BALANCE AND RENEWABLES USE

%sto_res_only%$ontext
energy_balance(h)..
         sum( ct , G_CON(ct,h)) + sum( res , G_RES(res,h)) + sum( sto , STO_OUT(sto,h))
         + G_INFEAS(h)
         =E= d(h)
;

renewable_generation(res,h)..
         phi_res(res,h) * N_RES(res)
         =E= G_RES(res,h) + CU(res,h) + sum( sto , STO_IN(sto,h))
%not_p2x%        + sum( p2x , P2X_IN(p2x,h))
;
$ontext
$offtext

%sto_anything%$ontext
energy_balance(h)..
         sum( ct , G_CON(ct,h)) + sum( res , G_RES(res,h)) + sum( sto , STO_OUT(sto,h))
         + G_INFEAS(h)
         =E= d(h) + sum( sto , STO_IN(sto,h) )
;

renewable_generation(res,h)..
         phi_res(res,h) * N_RES(res) =E= G_RES(res,h) + CU(res,h)
;
$ontext
$offtext

*-------------------------------------------------------------------------------
** MINIMUM RES SHARE

* RES share constraint (1a)
%rescon_1a%$ontext
minRES..
        sum( (res,h) , G_RES(res,h) )
        =G= phi_min_res/100 * sum( h, d(h) )
;
$ontext
$offtext

* RES share constraint (1b)
%rescon_1b%$ontext
minRES..
        sum( (res,h) , G_RES(res,h) )
        =G= phi_min_res/100 * sum( h, d(h) + sum(sto, STO_IN(sto,h) - STO_OUT(sto,h)) )
;
$ontext
$offtext

* RES share constraint (1c)
%rescon_1c%$ontext
minRES..
        sum( (res,h) , G_RES(res,h) )
        =G= phi_min_res/100 * sum(h, d(h)) + sum( (sto,h), STO_IN(sto,h) - STO_OUT(sto,h))
;
$ontext
$offtext

* RES share constraint (2a)
%rescon_2a%$ontext
minRES..
        sum( (res,h) , G_RES(res,h) )
        =G= phi_min_res/100 * sum( h, sum(res, G_RES(res,h)) + sum(ct, G_CON(ct,h))) - phi_min_res/100 * sum( (sto,h), STO_IN(sto,h) - STO_OUT(sto,h))
;
$ontext
$offtext

* RES share constraint (2b)
%rescon_2b%$ontext
minRES..
        sum( (res,h) , G_RES(res,h) )
        =G= phi_min_res/100 * sum( h, sum(res, G_RES(res,h)) + sum(ct, G_CON(ct,h)) )
;
$ontext
$offtext


* RES share constraint (2c)
%rescon_2c%$ontext
minRES..
        sum( (res,h) , G_RES(res,h) )
        =G= phi_min_res/100 * sum( h, sum(res, G_RES(res,h)) + sum(ct, G_CON(ct,h))) + (1-phi_min_res/100) * sum( (sto,h), STO_IN(sto,h) - STO_OUT(sto,h))
;
$ontext
$offtext

* RES share constraints (3a)
%rescon_3a%$ontext
minRES..
        sum( (ct,h), G_CON(ct,h) ) * (-1)
        =G= ((1-phi_min_res/100) * sum( h, d(h) ) + sum( (sto,h), STO_IN(sto,h) - STO_OUT(sto,h))) * (-1)
;
$ontext
$offtext

* RES share constraint (3b)
%rescon_3b%$ontext
minRES..
        sum( (ct,h), G_CON(ct,h) ) * (-1)
        =G= (1-phi_min_res/100) * sum( h, d(h) + sum( sto, STO_IN(sto,h) - STO_OUT(sto,h)) ) * (-1)
;
$ontext
$offtext

* RES share constraint (3c)
%rescon_3c%$ontext
minRES..
        sum( (ct,h), G_CON(ct,h) ) * (-1)
        =G= (1-phi_min_res/100) * sum( h, d(h) ) * (-1)
;
$ontext
$offtext

* RES share constraint (4a)
%rescon_4a%$ontext
minRES..
        sum( (ct,h), G_CON(ct,h) ) * (-1)
        =G= ((1-phi_min_res/100) * sum( h, sum( ct, G_CON(ct,h)) + sum(res, G_RES(res,h)) )
            + phi_min_res/100 * sum( (sto,h), STO_IN(sto,h) - STO_OUT(sto,h))) * (-1)
;
$ontext
$offtext


* RES share constraint (4b)
%rescon_4b%$ontext
minRES..
        sum( (ct,h), G_CON(ct,h) ) * (-1)
        =G= (1-phi_min_res/100) * sum( h, sum( res, G_RES(res,h)) + sum(ct, G_CON(ct,h)) ) * (-1)
;
$ontext
$offtext

* RES share constraint (4c)
%rescon_4c%$ontext
minRES..
        sum( (ct,h), G_CON(ct,h) ) * (-1)
        =G= ((1-phi_min_res/100) * sum( h, sum( res, G_RES(res,h)) + sum(ct, G_CON(ct,h)) )
             - (1-phi_min_res/100) * sum( (sto,h), STO_IN(sto,h) - STO_OUT(sto,h))) * (-1)
;
$ontext
$offtext

* CO2 constraint (5a): CO2 budget with implicit CO2 price
%co2con_5a%$ontext
co2Budget..
        sum( (ct,h), G_CON(ct,h) * emission_factor(ct)) * (-1)
        =G= co2_budget * (-1)
;
$ontext
$offtext


*-------------------------------------------------------------------------------
** CONVENTIONAL GENERATION

maximum_generation_con(ct,h)..
         G_CON(ct,h) =L= N_CON(ct)
;

*-------------------------------------------------------------------------------
** STORAGE

stolev(sto,h)$( ord(h) > 1 )..
         STO_L(sto,h) =E= STO_L(sto,h-1) + STO_IN(sto,h) * eta_sto_in(sto) - STO_OUT(sto,h)/eta_sto_out(sto)
;

stolev_start_end(sto)..
         STO_L(sto,'h1') =E= STO_L(sto,%end_hour%)
;

stolev_max(sto,h)..
        STO_L(sto,h) =L= N_STO_E(sto)
;

maxin_power(sto,h)..
         STO_IN(sto,h) =L= N_STO_P_IN(sto)
;

maxout_power(sto,h)..
         STO_OUT(sto,h) =L= N_STO_P_OUT(sto)
;


*-------------------------------------------------------------------------------
** POWER-TO-X

%p2x%$ontext
maxin_p2x(p2x,h)..
         P2X_IN(p2x,h) =L= p2x_power(p2x)
;

flh_p2x(p2x)..
         sum( h , P2X_IN(p2x,h)) =E= p2x_flh(p2x) * p2x_power(p2x)
;
$ontext
$offtext

*_______________________________________________________________________________


Model dieterpy_reduced /
objective

energy_balance
renewable_generation
%rescon_1a%%rescon_1b%%rescon_1c%%rescon_2a%%rescon_2b%%rescon_2c%%rescon_3a%%rescon_3b%%rescon_3c%%rescon_4a%%rescon_4b%%rescon_4c%$ontext
minRES
$ontext
$offtext
%co2con_5a%$ontext
co2Budget
$ontext
$offtext

maximum_generation_con

%max_loss%$ontext
maximum_loss
$ontext
$offtext

%max_curtailment%$ontext
maximum_curtailment
$ontext
$offtext

stolev_start_end
stolev
stolev_max

maxin_power
maxout_power

%not_p2x%maxin_p2x
%not_p2x%flh_p2x
/
;

*_______________________________________________________________________________

********************************************************************************
***** Options, fixings, report preparation *****
********************************************************************************

* Solver options
$onecho > cplex.opt
lpmethod 4
threads %thread%
epgap 1e-3
epagap 10
parallelmode -1
eprhs 1e-5
*feasopt 1
*equations.feaspref 1
$offecho


%no_crossover%$ontext
$onecho > cplex.opt
lpmethod 4
threads %thread%
epgap 1e-3
SolutionType 2
barepcomp 1e-8
eprhs 1e-5
*feasopt 1
*equations.feaspref 1
$offecho
$ontext
$offtext

* .OptFile: Do not forget to 'tell' the solver to read the options file by adding the following line:
dieterpy_reduced.OptFile = 1;
* .holdFixed: 1 Fixed variables are treated as constants, 0 Fixed variables are not treated as constants.
dieterpy_reduced.holdFixed = 1 ;