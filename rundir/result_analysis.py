# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 18:06:58 2020

@author: mkittel

questions best pratices:
    - call method in __init__
    - when computing e.g. scen_list: return only list or save as attribute (=issue for storage?), or self.storage_generation = df_sto
    - see: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html for masking, where, query

TODO:
    - Units of marginals
    - Sto charge and discharge w/ marginal
 
"""

import pandas as pd
import numpy as np
from matplotlib import rcParams
from matplotlib import pyplot as plt
import plotly.graph_objects as go
import plotly.io as pio
pio.renderers.default = 'firefox'
import webbrowser
firefox_path='C:\\Program Files\\Mozilla Firefox\\firefox.exe'
webbrowser.register('firefox', None,webbrowser.BackgroundBrowser(firefox_path),1)
from plotly.offline import plot
import seaborn as sns
import re


class MGamsPyAnalysis():

    # Constructor
    def __init__(self, x_report_csv_path):
        '''
        Instantiates analysis object for mgamspy. Imports model results.

        Parameters
        ----------
        x_report_csv_path: string
            File path to report.csv.

        Returns
        -------
        Object instance of class MGamsPyAnalysis.

        '''
        try:
            self.report_csv_path = x_report_csv_path
            self.resultfolder = []
            self.iter_constraints = []
            self.iter_parameters = {}
            self.scen_names_wish_list = []
            self.importReportCsv()
        except:
            raise
            print('E:__init__')

    def resultList2Matrix(self, results, col_idx=None, col_name=None):
        '''
        Transforms list of results (as dataframe) to matrix with rows refering
        to iter_parameter variations and columns to iter_constraints. Fills
        NaNs with 0.

        Parameters
        ----------
        results : pandas DataFrame
            Results of scenarios of format "parameter-value_constraint-name",
            separated by "_". Up to three digits possible for parameter_value,
            scenario-name imperatively needs to contain two digits.
        col_idx : int, optional
            Index of column (begins with 0). The default is None.
        col_name : str, optional
            Name of column. The default is None.

        Returns
        -------
        df : pandas DataFrame
            Constains results in matrix (parameter variations x constraint
            variation).
        '''
        try:
            df = pd.DataFrame(index=list(self.iter_parameters.values())[0], columns=self.iter_constraints)
            df.sort_index(inplace=True, ascending=False)
            
            # TODO: determine number of "_" characters if there are multiple parameters iterated and adjust assignment
            if col_idx is not None:
                for scen, row in results.iterrows():
                    if scen[1] == '_':
                        df.loc[float(scen[0]), scen[-2:]] = row.values[col_idx]
                    elif scen[2] == '_':
                        df.loc[float(scen[:2]), scen[-2:]] = row.values[col_idx]
                    elif scen[3] == '_':
                        df.loc[float(scen[:3]), scen[-2:]] = row.values[col_idx]
                    elif scen[4] == '_':
                        df.loc[float(scen[:4]), scen[-2:]] = row.values[col_idx]
            elif col_name is not None:
                for scen, row in results.iterrows():
                    if scen[1] == '_':
                        df.loc[float(scen[0]), scen[-2:]] = row[col_name]
                    elif scen[2] == '_':
                        df.loc[float(scen[:2]), scen[-2:]] = row[col_name]
                    elif scen[3] == '_':
                        df.loc[float(scen[:3]), scen[-2:]] = row[col_name]
                    elif scen[4] == '_':
                        df.loc[float(scen[:4]), scen[-2:]] = row[col_name]

            df.fillna(0, inplace=True)
            return df
        except:
            raise
            print('E:resultList2Matrix')

    def indexListNumber2Matrix(self, results, only_list=None):
        '''
        Transforms dictionary with indicies as value for each scenario 
        (key) to matrix holding the number of with rows refering to iter_parameter variations and columns to iter_constraints. Fills
        NaNs with 0.

        Parameters
        ----------
        results : dict
            Holds lists of occurences of storage cycling.

        Returns
        -------
        df_list: DataFrame
            Matrix with list of hourly indices of storage cycling 
            occurences.
        df_number: DataFrame
            Matrix with total number of occurences of storage cycling.

        '''
        try:
            df_list = pd.DataFrame(index=list(self.iter_parameters.values())[0], 
                                   columns=self.iter_constraints)
            df_list.sort_index(inplace=True, ascending=False)
            
            if not only_list:
                df_number = pd.DataFrame(index=list(self.iter_parameters.values())[0],
                                         columns=self.iter_constraints)
                df_number.sort_index(inplace=True, ascending=False)
            
            # TODO: determine number of "_" characters if there are multiple parameters iterated and adjust assignment
            for scen, list_hours in results.items():
                if scen[1] == '_':
                    df_list.loc[float(scen[0]), scen[-2:]] = list_hours
                    if not only_list:
                        df_number.loc[float(scen[0]), scen[-2:]] = len(list_hours)
                elif scen[2] == '_':
                    df_list.loc[float(scen[:2]), scen[-2:]] = list_hours
                    if not only_list:
                        df_number.loc[float(scen[:2]), scen[-2:]] = len(list_hours)
                elif scen[3] == '_':
                    df_list.loc[float(scen[:3]), scen[-2:]] = list_hours
                    if not only_list:
                        df_number.loc[float(scen[:3]), scen[-2:]] = len(list_hours)
                elif scen[4] == '_':
                    df_list.loc[float(scen[:4]), scen[-2:]] = list_hours
                    if not only_list:
                        df_number.loc[float(scen[:4]), scen[-2:]] = len(list_hours)

            df_list.fillna(0, inplace=True)
            if not only_list:
                df_number.fillna(0, inplace=True)
                return df_list, df_number
            return df_list
        except:
            raise
            print('E:indexListNumber2Matrix')
                        

    def importReportCsv(self):
        '''
        Imports model results from mgamspy and assigns data to object
        attribute 'model_results'.

        Returns
        -------
        None.

        '''
        try:
            self.model_results = pd.read_csv(self.report_csv_path)
        except:
            raise
            print('E:importReportCsv()')

    def createScenarioList(self, kind='matrix'):
        '''
        Creates scenario list based on iteration parameter names and values,
        and iteration constraints.

        Imperatively enter iter_parameters as dict with list of values per key:
        iter_parameters = {'parameter': [val1, val2, ...]}

        TODO: what if iteration parameter or constraints are empty?

        Returns
        -------
        scen_list: list
            Contains list of all scenario names.
        '''
        try:
            #iter_para_list = ['{}_{}'.format(k, v) for k, val_list in self.iter_parameters.items() for v in val_list]
            
            if kind == 'matrix':
                iter_para_list = ['{}'.format(v) for k, val_list in self.iter_parameters.items() for v in val_list]
                scen_names_wish_list = ['{}_{}'.format(par, con) for par in iter_para_list for con in self.iter_constraints]
                self.scen_names_wish_list = scen_names_wish_list
                return scen_names_wish_list
            elif kind == 'tuple':
                # list of all parameter values to be combined in one scenario
                combilist = []
                for combi in zip(*self.iter_parameters.values()):
                        combilist.append(combi)
                # list of all parameter names to be combined in one scenario
                paralist = list(self.iter_parameters.keys())
                # combine parameter names with values in one tuple dictionary
                scenario_tuples = []
                for combi in combilist:
                    tuple_dc = {}
                    for idx, value in enumerate(combi):
                        tuple_dc[paralist[idx]] = value
                    scenario_tuples.append(tuple_dc)
                
                #TODO: scen_names_wish_list = ['{}_{}'.format(par, con) for par in iter_para_list for con in self.iter_constraints]
                
        except:
            raise
            print('E:createScenarioList\n')

    def getScenarioList(self):
        '''
        Returns all scenario names in the report.csv and assigns it to object
        attribute 'scen_list'.

        Returns
        -------
        scen_list: list of strings
            Scenario names.

        '''
        try:
            self.scen_list = self.model_results.scenario.unique().tolist()
            return self.scen_list
        except:
            print('E:getScenarioList')
            raise

    def replaceScenarioNames(self):
        '''
        TODO

        Returns
        -------
        None.

        '''
        try:
            df = self.model_results.copy()        
            replacement_dc = dict(zip(self.scen_list, self.scen_names_wish_list))
            df['scenario'].replace(replacement_dc, inplace=True)
            self.scen_list = self.scen_names_wish_list
            self.model_results = df
        except:
            raise
            print('E:replaceScenarioNames')

    def getTotalSystemCosts(self):
        '''
        unit: bn EUR

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'Z']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': 'Total System Cost [bn EUR]'}, inplace=True)
            df.drop(df.columns.difference(['Total System Cost [bn EUR]']),
                    axis=1, inplace=True)

            df /= 1e9
            obj_val = self.resultList2Matrix(df, col_idx=0)

            self.obj_val = obj_val
            return self.obj_val
        except:
            raise
            print('E:getTotalSystemCosts')

    def getRESInvestmentCosts(self):
        '''
        Unit: EUR

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            # get specific investment cost for res [EUR/MW]
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'c_i_res']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': 'c_i_res [EUR/MW]', 'res': 'tech'}, inplace=True)
            df.drop(df.columns.difference(['c_i_res [EUR/MW]', 'tech']),
                    axis=1, inplace=True)
            c_i_res = {}
            for tech, group in df.groupby('tech'):
                c_i_res[tech] = self.resultList2Matrix(group['c_i_res [EUR/MW]'].to_frame(), col_idx=0)

            # get installed capacities [GW]
            if not hasattr(self, 'res_capacity'):
                res_capacity = self.getRESCapacity()
            else:
                res_capacity = self.res_capacity.copy()
            
            # get investment cost [EUR]
            res_cost_invest = {}
            for tech in res_capacity.keys():
                res_cost_invest[tech] = c_i_res[tech] * res_capacity[tech] * 1e3
            
            self.res_cost_invest = res_cost_invest
            return self.res_cost_invest

        except:
            raise
            print('E:getRESInvestmentCosts')


    def getConInvestmentCosts(self):
        '''
        unit: EUR

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            # get specific investment cost for conventionals [EUR/MW]
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'c_i_con']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': 'c_i_con [EUR/MW]', 'ct': 'tech'}, inplace=True)
            df.drop(df.columns.difference(['c_i_con [EUR/MW]', 'tech']),
                    axis=1, inplace=True)
            c_i_con = {}
            for tech, group in df.groupby('tech'):
                c_i_con[tech] = self.resultList2Matrix(group['c_i_con [EUR/MW]'].to_frame(), col_idx=0)
                
            # get installed capacities [GW]
            if not hasattr(self, 'con_capacity'):
                con_capacity = self.getConCapacity()
            else:
                con_capacity = self.con_capacity.copy()
            
            # get investment cost [EUR]  
            con_cost_invest = {}
            for tech in con_capacity.keys():
                con_cost_invest[tech] = c_i_con[tech] * con_capacity[tech] * 1e3
            
            self.con_cost_invest = con_cost_invest
            return self.con_cost_invest
        except:
            raise
            print('E:getConInvestmentCosts')

    def getConOperationalCosts(self):
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'c_var_con']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': 'c_var_con [EUR/MWh]', 'ct': 'tech'}, inplace=True)
            df.drop(df.columns.difference(['c_var_con [EUR/MWh]', 'tech']),
                    axis=1, inplace=True)
            c_var_con = {}
            for tech, group in df.groupby('tech'):
                c_var_con[tech] = self.resultList2Matrix(group['c_var_con [EUR/MWh]'].to_frame(), col_idx=0)
            
            # get generation
            if not hasattr(self, 'con_gen_total'):
                con_gen_total = self.getConGeneration('total')
            else:
                con_gen_total = self.con_gen_total.copy()  
            
            # get operational costs [EUR]
            con_cost_operation = {}
            for tech in con_gen_total.keys():
                con_cost_operation[tech] = c_var_con[tech] * con_gen_total[tech] * 1e6
                
            self.con_cost_operation = con_cost_operation
            return self.con_cost_operation
            
        except:
            raise
            print('E:getConOperationalCosts')
            
    def getStorageOperationalCosts(self):
        try:
            df = x.model_results.copy()
            df = df[df['Symbol'] == 'c_var_sto']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': 'c_var_sto [EUR/MWh]'}, inplace=True)
            df.drop(df.columns.difference(['c_var_sto [EUR/MWh]']),
                    axis=1, inplace=True)
            c_var_sto = x.resultList2Matrix(df, col_idx=0)
                       
            # get generation
            if not hasattr(self, 'sto_total_charge'):
                sto_total_charge = self.getStorageCharge('total')
            else:
                sto_total_charge = self.sto_total_charge.copy()
            if not hasattr(self, 'sto_total_discharge'):
                sto_total_discharge = self.getStorageDischarge('total')
            else:
                sto_total_discharge = self.sto_total_discharge.copy()
            
            # get operational costs [EUR]
            self.sto_cost_operation = (sto_total_charge + sto_total_discharge) * 1e6 * c_var_sto                
            return self.sto_cost_operation
        except:
            raise
            print('E:getStorageOperationalCosts')

    def getStorageEnergyInvestmentCost(self):
        '''
        unit: bn EUR

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            # get specific investment cost for storage energy [EUR/MWh]
            c_i_sto_e = self.model_results.copy()
            c_i_sto_e = c_i_sto_e[c_i_sto_e['Symbol'] == 'c_i_sto_e']
            c_i_sto_e.set_index('scenario', inplace=True)
            c_i_sto_e.rename(columns={'Level': 'c_i_sto_e [EUR/MWh]'}, inplace=True)
            c_i_sto_e.drop(c_i_sto_e.columns.difference(['c_i_sto_e [EUR/MWh]']),
                    axis=1, inplace=True)
            c_i_sto_e = self.resultList2Matrix(c_i_sto_e, col_idx=0)

            # get installed capacities [GWh]
            if not hasattr(self, 'sto_cap_e'):
                sto_cap_e = self.getStorageCapacityEnergy()
            else:
                sto_cap_e = self.sto_cap_e.copy()
            
            # get investment cost [EUR]
            sto_cost_invest_e = c_i_sto_e * sto_cap_e * 1e3
            self.sto_cost_invest_e = sto_cost_invest_e
            return self.sto_cost_invest_e
        except:
            raise
            print('E:getStorageEnergyInvestmentCost')

    def getStoragePowerInvestmentCost(self, kind):
        '''
        unit: EUR

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            # get specific investment cost for storage energy [EUR/MW]
            c_i_sto_p = self.model_results.copy()
            c_i_sto_p = c_i_sto_p[c_i_sto_p['Symbol'] == 'c_i_sto_p']
            c_i_sto_p.set_index('scenario', inplace=True)
            c_i_sto_p.rename(columns={'Level': 'c_i_sto_p [EUR/MW]'}, inplace=True)
            c_i_sto_p.drop(c_i_sto_p.columns.difference(['c_i_sto_p [EUR/MW]']),
                    axis=1, inplace=True)
            c_i_sto_p = self.resultList2Matrix(c_i_sto_p, col_idx=0)
            
            # get installed capacities [GW]
            if kind == 'IN':    
                if not hasattr(self, 'sto_cap_p_in'):
                    sto_cap_p_in = self.getStorageCapacityPower('IN')
                else:
                    sto_cap_p_in = self.sto_cap_p_in.copy()
                # get investment cost [bn EUR]
                sto_cost_invest_p_in = c_i_sto_p * sto_cap_p_in * 1e3
                self.sto_cost_invest_p_in = sto_cost_invest_p_in
                return self.sto_cost_invest_p_in
            
            elif kind == 'OUT':
                if not hasattr(self, 'sto_cap_p_out'):
                    sto_cap_p_out = self.getStorageCapacityPower('OUT')
                else:
                    sto_cap_p_out = self.sto_cap_p_out.copy()
                # get investment cost [EUR]
                sto_cost_invest_p_out = c_i_sto_p * sto_cap_p_out * 1e3
                self.sto_cost_invest_p_out = sto_cost_invest_p_out
                return self.sto_cost_invest_p_out
        except:
            raise
            print('E:getStoragePowerInvestmentCost')
            
    def getRESLCOE(self):
        try:
            if not hasattr(self, 'res_cost_invest'):
                res_cost_invest = self.getRESInvestmentCosts()
            else:
                res_cost_invest = self.res_cost_invest.copy()
            if not hasattr(self, 'res_gen_total'):
                res_gen_total = self.getRESGeneration('total')
            else:
                res_gen_total = self.res_gen_total.copy()
            lcoe_res = {}
            for tech in res_gen_total.keys():
                lcoe_res[tech] = res_cost_invest[tech].div(res_gen_total[tech] * 1e6, fill_value=0)
            self.lcoe_res = lcoe_res
            return self.lcoe_res
        except:
            raise
            print('E:getRESLCOE')
            
    def getConLCOE(self):
        try:
            if not hasattr(self, 'con_cost_invest'):
                con_cost_invest = self.getConInvestmentCosts()
            else:
                con_cost_invest = self.con_cost_invest.copy()
            if not hasattr(self, 'con_cost_operation'):
                con_cost_operation = self.getConOperationalCosts()
            else:
                con_cost_operation = self.con_cost_operation.copy()
            if not hasattr(self, 'con_gen_total'):
                con_gen_total = self.getConGeneration('total')
            else:
                con_gen_total = self.con_gen_total.copy()
            lcoe_con = {}
            for tech in con_gen_total.keys():
                numerator = con_cost_invest[tech] + con_cost_operation[tech]
                lcoe_con[tech] = numerator.div(con_gen_total[tech] * 1e6, fill_value=0)
            self.lcoe_con = lcoe_con
            return self.lcoe_con
        except:
            raise
            print('E:getConLCOE')
            
    def getStorageLCOS(self):
        try:
            if not hasattr(self, 'sto_cost_invest_e'):
                sto_cost_invest_e = self.getStorageEnergyInvestmentCost()
            else:
                sto_cost_invest_e = self.sto_cost_invest_e.copy()
            if not hasattr(self, 'sto_cost_invest_p_in'):
                sto_cost_invest_p_in = self.getStoragePowerInvestmentCost('IN')
            else:
                sto_cost_invest_p_in = self.sto_cost_invest_p_in.copy()
            if not hasattr(self, 'sto_cost_invest_p_out'):
                sto_cost_invest_p_out = self.getStoragePowerInvestmentCost('OUT')
            else:
                sto_cost_invest_p_out = self.sto_cost_invest_p_out.copy()
            if not hasattr(self, 'sto_cost_operation'):
                sto_cost_operation = self.getStorageOperationalCosts()
            else:
                sto_cost_operation = self.sto_cost_operation.copy()
            if not hasattr(self, 'sto_total_discharge'):
                sto_total_discharge = self.getStorageDischarge('total')
            else:
                sto_total_discharge = self.sto_total_discharge.copy()
            
            numerator = sto_cost_invest_e + sto_cost_invest_p_in + sto_cost_invest_p_out + sto_cost_operation
            lcos = numerator.div(sto_total_discharge * 1e6, fill_value=0)
            self.storage_lcos = lcos
            return self.storage_lcos
        except:
            raise
            print('E:getStorageLCOS')

    def getShadowPrices(self, constraint='energy_balance'):
        '''
        Retrieves shadow price of constraint defined by kind parameter.

        Parameters
        ----------
        kind : string, optional
            Constraint from which the shadow price is to be retrieved. The 
            default is 'energy_balance'.

        Returns
        -------
        df : pandas Series
            All shadow prices across all scenarios.

        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == constraint]
            df.set_index('h', inplace=True)
            df.index = df.index.astype('int')
            df.drop(df.columns.difference(['Marginal', 'scenario']), axis=1, inplace=True)    
            if constraint == 'energy_balance':
                df.rename(columns={'Marginal': 'Electricity Price [EUR/MWh]'}, inplace=True)
                self.el_prices = df
            else:
                df.rename(columns={'Marginal': 'Shadow Price [EUR/MWh]'}, inplace=True)
            return df
        except:
            raise
            print('E:getShadowPrices')
    
    def getNegativePrices(self, shadow_prices, kind='number'):
        '''
        possible configurations:
            kind='number': number of hours with negative prices
            kind='list': list of indeces of hours with negative prices
            kind='prices': dict of negative prices with hourly indeces

        Parameters
        ----------
        shadow_prices : dataframe or series
            Holds all shadow prices.
        kind : string, optional
            Configures output. The default is 'number'.

        Returns
        -------
        Dataframe or dict
            Holds desired output for assessment of negative prices.

        '''
        try:
            neg_prices_hours_dc = {}
            neg_prices_dc = {}
            for scen, group in shadow_prices.groupby('scenario'):
                mask = group['Electricity Price [EUR/MWh]'] <= 0
                neg_prices_hours_dc[scen] = mask[mask].index.values.astype(int).tolist()
                neg_prices_dc[scen] = group[mask]['Electricity Price [EUR/MWh]']
            
            neg_prices_list = pd.DataFrame(index=list(self.iter_parameters.values())[0],
                                           columns=self.iter_constraints)
            neg_prices_number = pd.DataFrame(index=list(self.iter_parameters.values())[0],
                                             columns=self.iter_constraints)
                    
            for scen, list_hours in neg_prices_hours_dc.items():
                if scen[1] == '_':
                    neg_prices_list.loc[float(scen[0]), scen[-2:]] = list_hours
                    neg_prices_number.loc[float(scen[0]), scen[-2:]] = len(list_hours)
                elif scen[2] == '_':
                    neg_prices_list.loc[float(scen[:2]), scen[-2:]] = list_hours
                    neg_prices_number.loc[float(scen[:2]), scen[-2:]] = len(list_hours)
                elif scen[3] == '_':
                    neg_prices_list.loc[float(scen[:3]), scen[-2:]] = list_hours
                    neg_prices_number.loc[float(scen[:3]), scen[-2:]] = len(list_hours)
                elif scen[4] == '_':
                    neg_prices_list.loc[float(scen[:4]), scen[-2:]] = list_hours
                    neg_prices_number.loc[float(scen[:4]), scen[-2:]] = len(list_hours)
        
            neg_prices_list.sort_index(inplace=True, ascending=False)
            neg_prices_number.sort_index(inplace=True, ascending=False)
                    
            # assign attributes
            self.neg_prices_list = neg_prices_list
            self.neg_prices_number = neg_prices_number
            self.neg_prices = neg_prices_dc
            
            if kind == 'number':
                return self.neg_prices_number
            elif kind == 'list':
                return self.neg_prices_list
            elif kind == 'prices':
                return self.neg_prices
        except:
            raise
            print('E:getNegativePrices')              

    def getDemand(self, kind='hourly'):
        '''
        Unit for hourly demand: MWh
        Unit for total demand: TWh
        '''
        try:
            if kind == 'hourly':
                df = self.model_results.copy()
                df = df[df['Symbol'] == 'd']
                df.set_index('h', inplace=True)
                df.index = df.index.astype('int')
                df.rename(columns={'Level': 'd [MW]'}, inplace=True)
                df.drop(df.columns.difference(['d [MW]', 'scenario']), axis=1,
                        inplace=True)
                self.hourly_demand = df
                return self.hourly_demand

            elif kind == 'hourly_scen':
                if not hasattr(self, 'hourly_demand'):
                    df = self.getDemand('hourly')
                else:
                    df = self.getDemand('hourly').copy()
                hourly_demand_scen = {}
                for scen, group in df.groupby('scenario'):
                    hourly_demand_scen[scen] = group['d [MW]']
                self.hourly_demand_scen = hourly_demand_scen
                return self.hourly_demand_scen

            elif kind == 'total':
                if not hasattr(self, 'hourly_demand'):
                    df = self.getDemand('hourly')
                else:
                    df = self.getDemand('hourly').copy()

                demand = df.groupby('scenario').sum()
                demand /= 1e6
                demand_total = self.resultList2Matrix(demand, col_idx=0)

                self.demand_total = demand_total
                return self.demand_total
        except:
            raise
            print('E:getDemand')

    def getConGeneration(self, kind='hourly'):
        '''
        Unit for hourly generation: MWh
        Unit for total generation: TWh

        Parameters
        ----------
        kind : TYPE, optional
            DESCRIPTION. The default is 'hourly'.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if kind == 'hourly':
                df = self.model_results.copy()
                df = df[df['Symbol'] == 'G_CON']
                df.set_index('h', inplace=True)
                df.index = df.index.astype('int')
                df.rename(columns={'ct': 'tech', 'Level': 'G_CON.l [MW]',
                                   'Marginal': 'G_CON.m [EUR/MW]'}, inplace=True)
                df.drop(df.columns.difference(['tech', 'G_CON.l [MW]',
                                               'G_CON.m [EUR/MW]', 'scenario']),
                        axis=1, inplace=True)
                
                # group according to tech
                con_gen_hourly = {}
                for tech, group in df.groupby('tech'):
                    df = group[['G_CON.l [MW]', 'G_CON.m [EUR/MW]', 'scenario']]
                    con_gen_hourly[tech] = {scen: group for scen, group in df.groupby('scenario')}
                    
                # add hours with availability factor of one
                index = [i for i in range(1, 8761)]
                full_year = pd.Series(index=index, name='temp', dtype='float64')
            
                for tech, data_dc in con_gen_hourly.items():
                    for scen, df in data_dc.items():
                        _df = pd.concat([df, full_year], axis=1)
                        _df.fillna(0, inplace=True)
                        _df = _df['G_CON.l [MW]']
                        _df.rename(str('G_' + tech + '.l [MW]'), inplace=True)
                        con_gen_hourly[tech][scen] = _df
                
                # add scenarios if missing
                for tech, data_dc in con_gen_hourly.items():
                    for scen in self.scen_list:
                        if scen not in data_dc.keys():
                            con_gen_hourly[tech][scen] = full_year.rename(str('G_' + tech + '.l [MW]'))
                
                self.con_gen_hourly = con_gen_hourly
                return self.con_gen_hourly

            elif kind == 'total':
                if not hasattr(self, 'con_gen_hourly'):
                    con_gen_hourly = self.getConGeneration('hourly')
                else:
                    con_gen_hourly = self.con_gen_hourly.copy()
            
            # aggregate hourly values to annual energy amount
            con_gen_total = {}
            for tech, data_dc in con_gen_hourly.items():
                con_gen_total[tech] = {}
                for scen, s in data_dc.items():
                    con_gen_total[tech][scen] = s.sum()
            
            # convert to DataFrame and from MW to TWh
            for tech, data_dc in con_gen_total.items():
                df_temp = pd.DataFrame.from_dict(data_dc, orient='index', columns=[str('G_' + tech + '.l [TWh]')])
                con_gen_total[tech] = df_temp / 1e6
            
            # convert to df
            for tech, df in con_gen_total.items():
                con_gen_total[tech] = self.resultList2Matrix(df, col_idx=0)
            
            self.con_gen_total = con_gen_total
            return self.con_gen_total
        except:
            raise
            print('E:getConGeneration')

    def getRESGeneration(self, kind='hourly'):
        '''
        TODO: same structure as CON --> make new function getGeneration(type={RES, CON}, kind='hourly')
        maybe even for storage!
        
        Parameters
        ----------
        kind : TYPE, optional
            DESCRIPTION. The default is 'hourly'.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if kind == 'hourly':
                df = self.model_results.copy()
                df = df[df['Symbol'] == 'G_RES']
                df.set_index('h', inplace=True)
                df.index = df.index.astype('int')
                df.rename(columns={'res': 'tech', 'Level': 'G_RES.l [MW]',
                                   'Marginal': 'G_RES.m [EUR/MW]'}, inplace=True)
                df.drop(df.columns.difference(['tech', 'G_RES.l [MW]',
                                               'G_RES.m [EUR/MW]', 'scenario']),
                        axis=1, inplace=True)
                
                # group according to tech
                res_gen_hourly = {}
                for tech, group in df.groupby('tech'):
                     df = group[['G_RES.l [MW]', 'G_RES.m [EUR/MW]', 'scenario']]
                     res_gen_hourly[tech] = {scen: group for scen, group in df.groupby('scenario')}
                     
                # add hours with availability factor of one
                index = [i for i in range(1, 8761)]
                full_year = pd.DataFrame(index=index, columns=['temp'], dtype='float64')
                zeros = [0 for i in range(1,8761)]
                full_year_zeros = pd.Series(index=index, name='G_RES.l [MW]', data=zeros)
            
                for tech, data_dc in res_gen_hourly.items():
                    for scen, df in data_dc.items():
                        _df = pd.concat([df, full_year], axis=1)
                        _df.fillna(0, inplace=True)
                        _df = _df['G_RES.l [MW]']
                        _df.rename(str('G_' + tech + '.l [MW]'), inplace=True)
                        res_gen_hourly[tech][scen] = _df
            
                    # add scenarios if missing
                    for scen in self.scen_list:
                        if scen not in res_gen_hourly[tech].keys():
                            _s = full_year_zeros.copy()
                            _s.rename(str('G_' + tech + '.l [MW]'), inplace=True)
                            res_gen_hourly[tech][scen] = _s
                    
                self.res_gen_hourly = res_gen_hourly
                return self.res_gen_hourly

            elif kind == 'total':
                if not hasattr(self, 'res_gen_hourly'):
                    res_gen_hourly = self.getRESGeneration('hourly')
                else:
                    res_gen_hourly = self.res_gen_hourly.copy()
                
                # aggregate hourly values to annual energy amount
                res_gen_total = {}
                for tech, data_dc in res_gen_hourly.items():
                    res_gen_total[tech] = {}
                    for scen, s in data_dc.items():
                        res_gen_total[tech][scen] = s.sum()
                
                # convert to DataFrame and from MW to TWh
                for tech, data_dc in res_gen_total.items():
                    df_temp = pd.DataFrame.from_dict(data_dc, orient='index', columns=['G_RES.l [TWh]'])
                    res_gen_total[tech] = df_temp / 1e6
                
                # convert to df
                for tech, df in res_gen_total.items():
                    res_gen_total[tech] = self.resultList2Matrix(df, col_idx=0)
                
                self.res_gen_total = res_gen_total
                return self.res_gen_total
        except:
            raise
            print('E:getRESGeneration')
            
    def getRESAvailability(self):
        '''
        TODO: add option of leap year!
        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'phi_res']
            if df.empty:
                raise
                print('''Include parameter phi_min in report.csv by adjusting
                      the report_query on sheet control_variables in
                      config.xlsx.''')
            else:
                df.set_index('h', inplace=True)
                df.index = df.index.astype('int')
                df.rename(columns={'res': 'tech', 'Level': 'phi_res'}, inplace=True)
                df.drop(df.columns.difference(['tech', 'phi_res','scenario']), axis=1,
                        inplace=True)
                
            # group by tech
            res_avail_tech = {}
            for tech, group in df.groupby('tech'):
                res_avail_tech[tech] = group[['phi_res','scenario']]
            
            # group each tech and scenario in single df within dict
            res_avail_tech_scen = {}
            for tech, df in res_avail_tech.items():
                res_avail_tech_scen[tech] = {scen: group[['phi_res']] for scen, group in df.groupby('scenario')}

            # add hours with availability factor of one
            index = [i for i in range(1, 8761)]
            data = [0 for i in range(1,8761)]
            full_year = pd.DataFrame(index=index, data=data, columns=['temp'])
            
            for tech, data_dc in res_avail_tech_scen.items():
                for scen, df in data_dc.items():
                    _df = pd.concat([df, full_year], axis=1)
                    _df.fillna(0, inplace=True)
                    res_avail_tech_scen[tech][scen] = _df['phi_res']
            
            self.res_avail = res_avail_tech_scen
            return self.res_avail
        except:
            raise
            print('E:getRESAvailability')

    def getRESAvailableGeneration(self, kind='total'):
        '''
        kind options:
            'hourly': unit MW
            'total': unit TWh
        '''
        try:
            if not hasattr(self, 'res_avail'):
                res_avail = self.getRESAvailability()
            else:
                res_avail = self.res_avail.copy()
            if not hasattr(self, 'res_cap_series'):
                res_capacity = self.getRESCapacity('series')
            else:
                res_capacity = self.res_cap_series.copy()

            if kind == 'hourly':
                res_avail_gen_hourly = {}
                for tech, data_dc in res_avail.items():
                    res_avail_gen_hourly[tech] = {}
                    for scen, df in data_dc.items():
                        _s = df * res_capacity[tech][scen] * 1e3
                        _s.rename(str(tech + '_avail.l [MW]'), inplace=True)
                        res_avail_gen_hourly[tech][scen] = _s
                self.res_avail_gen_hourly = res_avail_gen_hourly
                return self.res_avail_gen_hourly           
            
            elif kind == 'total':
                if not hasattr(self, 'res_avail_gen_hourly'):
                    res_avail_gen_hourly = self.getRESAvailableGeneration('hourly')
                else:
                    res_avail_gen_hourly = self.res_avail_gen_hourly.copy()
                
                res_avail_gen_total = {}
                for tech, data_dc in res_avail_gen_hourly.items():
                    res_avail_gen_total[tech] = {}
                    for scen, s in data_dc.items():
                        res_avail_gen_total[tech][scen] = s.sum() / 1e6
                                        
                # convert to df
                for tech, data_dc in res_avail_gen_total.items():
                    temp = pd.DataFrame.from_dict(data_dc, orient='index')
                    res_avail_gen_total[tech] = self.resultList2Matrix(temp, col_idx=0)
                
                self.res_avail_gen_total = res_avail_gen_total
                return self.res_avail_gen_total
        except:
            raise
            print('E:getRESAvailableGeneration')

    def getStorageGeneration(self, kind='hourly'):
        '''
        draft: Filters storage generation data [MWh] from model results and assigns
        these data to object attribute 'storage_gen'

        Parameters
        ----------
        kind : TYPE, optional
            DESCRIPTION. The default is 'hourly'.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if kind == 'hourly':
                df = self.model_results.copy()
                df = self.model_results.copy()
                df_sto_in = df[df['Symbol'] == 'STO_IN']
                df_sto_out = df[df['Symbol'] == 'STO_OUT']
                df_sto = pd.merge(df_sto_in, df_sto_out, on=['h', 'scenario'],
                                  how='outer')
                df_sto.rename(columns={'Level_x': 'STO_IN.l [MW]',
                                       'Marginal_x': 'STO_IN.m [MW]',
                                       'Level_y': 'STO_OUT.l [MW]',
                                       'Marginal_y': 'STO_OUT.m [MW]'},
                              inplace=True)
                df_sto.set_index('h', inplace=True)
                df.index = df.index.astype('int')
                df_sto.drop(df_sto.columns.difference(['STO_IN.l [MW]', 'STO_IN.m [MW]',
                                                       'STO_OUT.l [MW]', 'STO_OUT.m [MW]',
                                                       'scenario']),
                            axis=1, inplace=True)
                self.sto_hourly_gen = df_sto
                return self.sto_hourly_gen

            elif kind == 'total':
                if not hasattr(self, 'stor_hourly_gen'):
                    df = self.getStorageGeneration('hourly')
                else:
                    df = self.sto_hourly_gen.copy()
                df = df.groupby('scenario')['STO_IN.l [MW]', 'STO_OUT.l [MW]'].sum()
                df /= 1e6
                df.rename(columns={'STO_IN.l [MW]': 'STO_IN_total.l [TWh]',
                                   'STO_OUT.l [MW]': 'STO_OUT_total.l [TWh]'},
                          inplace=True)

                sto_total_gen = {}

                for col in df:
                    sto_total_gen[col] = self.resultList2Matrix(df[col].to_frame(), col_idx=0)

                self.sto_total_gen = sto_total_gen
                return self.sto_total_gen
        except:
            raise
            print('E:getStorageGeneration')

    def getStorageLevel(self):
        '''
        TODO

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'STO_L']
            df.rename(columns={'Level': 'STO_L.l [MWh]',
                               'Marginal': 'STO_L.m [MWh]',},
                      inplace=True)
            df.set_index('h', inplace=True)
            df.index = df.index.astype('int')
            df.drop(df.columns.difference(['STO_L.l [MWh]', 'STO_L.m [MWh]',
                                           'scenario']),
                    axis=1, inplace=True)
            
            # add hours with availability factor of one
            index = [i for i in range(1, 8761)]
            data = [0 for i in range(1,8761)]
            full_year = pd.Series(index=index, data=data, name='temp')
                
            # group by scenario
            sto_level_hourly = {}
            for scen, group in df.groupby('scenario'):
                _s = group['STO_L.l [MWh]']
                _s = pd.concat([_s, full_year], axis=1)
                _s = _s['STO_L.l [MWh]'].fillna(0)
                sto_level_hourly[scen] = _s
            
            # add scenarios if missing
            for scen in self.scen_list:
                if scen not in sto_level_hourly.keys():
                    sto_level_hourly[scen] = full_year.rename('STO_L.l [MWh]')
            
            self.sto_level_hourly = sto_level_hourly
            return self.sto_level_hourly
        except:
            raise
            print('E:getStorageLevel')

    def getStorageCharge(self, kind='hourly'):
        '''
        Unit for hourly: MWh
        Unit for total: TWh

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if not hasattr(self, 'sto_hourly_gen'):
                df = self.getStorageGeneration('hourly')
            else:
                df = self.sto_hourly_gen.copy()
                
            if kind == 'hourly':    
                # add hours with availability factor of one
                index = [i for i in range(1, 8761)]
                data = [0 for i in range(1,8761)]
                full_year = pd.Series(index=index, data=data, name='temp')
                
                # group by scenario
                sto_charge_hourly = {}
                for scen, group in df.groupby('scenario'):
                    _s = group['STO_IN.l [MW]']
                    _s = pd.concat([_s, full_year], axis=1)
                    _s = _s['STO_IN.l [MW]'].fillna(0)
                    sto_charge_hourly[scen] = _s
                
                # add scenarios if missing
                for scen in self.scen_list:
                    if scen not in sto_charge_hourly.keys():
                        sto_charge_hourly[scen] = full_year.rename('STO_IN.l [MW]')
                
                self.sto_charge_hourly = sto_charge_hourly
                return self.sto_charge_hourly
            
            elif kind == 'total':
                if not hasattr(self, 'sto_total_gen'):
                    sto_gen_dc = self.getStorageGeneration('total')
                else:
                    sto_gen_dc = self.sto_total_gen.copy()
                self.sto_total_charge = sto_gen_dc['STO_IN_total.l [TWh]']
                return self.sto_total_charge
        except:
            raise
            print('E:getStorageCharge')

    def getStorageDischarge(self, kind='hourly'):
        '''
        Unit for hourly: MWh
        Unit for total: TWh
        
        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if kind == 'hourly':
                if not hasattr(self, 'sto_hourly_gen'):
                    df = self.getStorageGeneration('hourly')
                else:
                    df = self.sto_hourly_gen.copy()
                
                # add hours with availability factor of one
                index = [i for i in range(1, 8761)]
                data = [0 for i in range(1,8761)]
                full_year = pd.Series(index=index, data=data, name='temp')
                
                # group by scenario
                sto_discharge_hourly = {}
                for scen, group in df.groupby('scenario'):
                    _s = group['STO_OUT.l [MW]']
                    _s = pd.concat([_s,full_year], axis=1)
                    _s = _s['STO_OUT.l [MW]'].fillna(0)
                    sto_discharge_hourly[scen] = _s
                
                # add scenarios if missing
                for scen in self.scen_list:
                    if scen not in sto_discharge_hourly.keys():
                        sto_discharge_hourly[scen] = full_year.rename('STO_OUT.l [MW]')
                
                self.sto_discharge_hourly = sto_discharge_hourly
                return self.sto_discharge_hourly
            elif kind == 'total':
                if not hasattr(self, 'sto_total_gen'):
                    sto_gen_dc = self.getStorageGeneration('total')
                else:
                    sto_gen_dc = self.sto_total_gen.copy()
                self.sto_total_discharge = sto_gen_dc['STO_OUT_total.l [TWh]']
                return self.sto_total_discharge
        except:
            raise
            print('E:getStorageDischarge')

    def getRESCapacity(self, kind='compact'):
        '''
        TODO: harmonize with CON, sto_p --> getCapacity(class={RES,CON,STO}, type={power,energy})
        
        Unit for installed capacity: GW

        Filters installed RES capacity [GW] from model results and assigns
        these data to object attribute 'res_capacity'.

        Returns
        -------
        None.
        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'N_RES']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'res': 'tech', 'Level': 'N_RES.l [GW]', 'Marginal': 'N_RES.m [GW]'},
                      inplace=True)
            df.drop(df.columns.difference(['tech', 'N_RES.l [GW]', 'scenario']), #'N_RES.m [GW]'
                    axis=1, inplace=True)
            
            # get technology-specific capacities
            res_cap_dc = {}
            for tech, group in df.groupby('tech'):
                res_cap_dc[tech] = group
            res_cap_df = pd.DataFrame()
            for k, data in res_cap_dc.items():
                res_cap_df[k] = data['N_RES.l [GW]']
            
            # aggregate from MW to GW
            res_cap_df /= 1e3
            res_cap_series = {}
            
            # add missing scenarios
            for tech in res_cap_df:
                missing_scens = {} 
                for scen in self.scen_list:
                    if scen not in res_cap_df[tech].index:
                        missing_scens[scen] = 0
                missing_scens = pd.Series(missing_scens)                         
                res_cap_series[tech] = res_cap_df[tech].append(missing_scens)
            self.res_cap_series = res_cap_series
            
            # assign in dataframe structure into dict
            res_cap_compact = {}
            for idx, res in enumerate(res_cap_df):
                res_cap_compact[res] = self.resultList2Matrix(res_cap_df, col_idx=idx)
            self.res_cap_compact = res_cap_compact
            
            if kind == 'series':
                return self.res_cap_series
            elif kind == 'compact':
                return self.res_cap_compact
        except:
            raise
            print('E:getRESCapacity')

    def getConCapacity(self):
        '''
        Unit for installed capacity: GW

        Returns
        -------
        dict
            Contains technology-specific installed capacity for conventional
            generation technologies.

        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'N_CON']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'ct':'tech', 'Level': 'N_CON.l [GW]', 'Marginal': 'N_CON.m [GW]'},
                      inplace=True)
            df.drop(df.columns.difference(['N_CON.l [GW]', 'tech', 'scenario']), #'N_CON.m [GW]'
                    axis=1, inplace=True)

            # get technology-specific capacities
            cap_con_dc = {}
            for tech, group in df.groupby('tech'):
                cap_con_dc[tech] = group
            cap_con = pd.DataFrame()
            for k, data in cap_con_dc.items():
                cap_con[k] = data['N_CON.l [GW]']

            # aggregate from MW to GW
            cap_con /= 1e3

            # assign in dataframe structure into dict
            con_capacity = {}
            for idx, ct in enumerate(cap_con):
                con_capacity[ct] = self.resultList2Matrix(cap_con, col_idx=idx)

            self.con_capacity = con_capacity
            return self.con_capacity
        except:
            raise
            print('E:getConCapacity')

    def getStorageCapacityPower(self, kind):
        '''
        kind options:
            IN
            OUT
        
        Filters installed storage capacity [GW] from model results and assigns
        these data to object attribute 'storage_power'.

        Returns
        -------
        None.
        '''
        try:
            df = self.model_results.copy()
            name = str('N_STO_P_' + kind)
            df = df[df['Symbol'] == name]
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': str(name + '.l [GW]'), 'Marginal': str(name + '.m [GW]')},
                      inplace=True)
            df.drop(df.columns.difference([str(name + '.l [GW]'), 'scenario']), #'N_STO_P.m [GW]',
                    axis=1, inplace=True)
            
            # aggregate from MW to GW
            df /= 1e3

            sto_cap_p = self.resultList2Matrix(df, col_idx=0)
            
            if kind == 'IN':
                self.sto_cap_p_in = sto_cap_p
                return self.sto_cap_p_in
            elif kind == 'OUT':
                self.sto_cap_p_out = sto_cap_p
                return self.sto_cap_p_out
        except:
            raise
            print('E:getStorageCapacityPower')
            
    def getStorageCapacityEnergy(self):
        '''
        Filters available storage energy capacity [GWh] from model results and
        assigns these data to object attribute 'storage_energy'.

        Returns
        -------
        None.
        '''
        try:
            df = self.model_results.copy()
            df = df[df['Symbol'] == 'N_STO_E']
            df.set_index('scenario', inplace=True)
            df.rename(columns={'Level': 'N_STO_E.l [GWh]', 'Marginal': 'N_STO_E.m [GWh]'},
                      inplace=True)
            df.drop(df.columns.difference(['N_STO_E.l [GWh]', 'scenario']), # 'N_STO_E.m [GWh]'
                    axis=1, inplace=True)
            
            # aggregate from MWh to GWh
            df /= 1e3

            sto_cap_e = self.resultList2Matrix(df, col_idx=0)

            self.sto_cap_e = sto_cap_e
            return self.sto_cap_e
        except:
            raise
            print('E:getStorageCapacityEnergy')
            
    def getStorageE2P(self, kind='IN'):
        try:
            if not hasattr(self, 'sto_cap_e'):
                sto_cap_e = self.getStorageCapacityEnergy()
            else:
                sto_cap_e = self.sto_cap_e.copy()
            if kind == 'IN':
                if not hasattr(self, 'sto_cap_p_in'):
                    sto_cap_p_in = self.getStorageCapacityPower('IN')
                else:
                    sto_cap_p_in = self.sto_cap_p_in.copy()
                sto_e2p = round(sto_cap_e / sto_cap_p_in, 1)
                self.sto_e2p_in = sto_e2p
            if kind == 'OUT':
                if not hasattr(self, 'sto_cap_p_out'):
                    sto_cap_p_out = self.getStorageCapacityPower('OUT')
                else:
                    sto_cap_p_out = self.sto_cap_p_out.copy()
                sto_e2p = round(sto_cap_e / sto_cap_p_out, 1)
                self.sto_e2p_out = sto_e2p
            return sto_e2p
        except:
            print('E:getStorageE2P')
            raise


    def getStorageLoss(self):
        '''
        unit: TWh
        '''
        try:
            if not hasattr(self, 'sto_total_total_gen'):
                sto_total_total_gen = self.getStorageGeneration('total')
            else:
                sto_total_total_gen = self.sto_total_total_gen.copy()
            sto_loss = sto_total_total_gen['STO_IN_total.l [TWh]'].subtract(sto_total_total_gen['STO_OUT_total.l [TWh]'])
            self.sto_loss = sto_loss
            return self.sto_loss
        except:
            raise
            print('E:getStorageLoss')
            
    def getStorageNSL(self):
        try:
            if not hasattr(self, 'sto_total_discharge'):
                sto_total_discharge = self.getStorageDischarge('total')
            else:
                sto_total_discharge = self.sto_total_discharge.copy()
            if not hasattr(self, 'sto_loss'):
                sto_loss = self.getStorageLoss()
            else:
                sto_loss = self.sto_loss.copy()
            
            nsl = sto_loss.div(sto_total_discharge, fill_value=0)
            self.storage_nsl = nsl
            return self.storage_nsl
        except:
            raise
            print('E:getStorageNSL') 

    def getRESShare(self):
        try:
            if not hasattr(self, 'demand_total'):
                demand_total = self.getDemand('total')
            else:
                demand_total = self.demand_total.copy()
            if not hasattr(self, 'res_gen_total'):
                res_gen_total = self.getRESGeneration('total')
            else:
                res_gen_total = self.res_gen_total.copy()
                        
            res_shares = (sum(res_gen_total.values()) / demand_total) * 100
            self.res_shares = res_shares
            return self.res_shares
        except:
            raise
            print('E:getRESShare')
            
    def getConShare(self):
        try:
            if not hasattr(self, 'demand_total'):
                demand_total = self.getDemand('total')
            else:
                demand_total = self.demand_total.copy()
            if not hasattr(self, 'con_gen_total'):
                con_gen_total = self.getConGeneration('total')
            else:
                con_gen_total = self.con_gen_total.copy()            
            self.con_shares = (sum(con_gen_total.values()) / demand_total) * 100
            return self.con_shares
        except:
            raise
            print('E:getConShare')

    def getRESShareDeviation(self, threshold=0.01):
        '''
        Interpretation:
            positive deviation => res_share > res_share_target
            negative deviation => res_share < res_share_target

        Unit of deviation: %

        Parameters
        ----------
        res_share : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if not hasattr(self, 'res_shares'):
                res_shares = self.getRESShare()
            else:
                res_shares = self.res_shares.copy()   
            res_share_target = pd.DataFrame(index=res_shares.index, columns=res_shares.columns)
            for col in res_share_target:
                res_share_target[col] = res_share_target.index

            res_share_dev = res_shares - res_share_target
            res_share_dev[res_share_dev < threshold] = 0

            self.res_share_dev = res_share_dev
            return self.res_share_dev
        except:
            raise
            print('E:getRESShareDeviation')

    def getRESShareUsed(self):
        try:
            if not hasattr(self, 'res_avail_gen_total'):
                res_avail_gen_total = self.getRESAvailableGeneration('total')
            else:
                res_avail_gen_total = self.res_avail_gen_total.copy()
            if not hasattr(self, 'res_gen_total'):
                res_gen_total = self.getRESGeneration('total')
            else:
                res_gen_total = self.res_gen_total.copy()
                
            res_share_used = sum(res_gen_total.values()).truediv(sum(res_avail_gen_total.values())) * 100
            self.res_share_used = res_share_used
            return self.res_share_used
        except:
            raise
            print('E:getRESShareUsed')
            
    def getStorageEfficiency(self, kind):
        '''
        Parameters
        ----------
        kind : str
            Indicates conversion process. Options are:
            - charging efficiency: kind='eta_sto_in'
            - discharging efficiency: kind='eta_sto_out'
            - roundtrip efficiency: kind='eta_sto'.

        Returns
        -------
        pandas Series
            Hold efficiency for each scenario.

        '''
        try:
            df = self.model_results.copy()
            eta = df[df['Symbol'] == kind].copy()
            eta.set_index('scenario', inplace=True)
            eta.rename(columns={'Level': str(kind + ' [%]')}, inplace=True)
            eta.drop(eta.columns.difference([str(kind + ' [%]')]), axis=1, 
                     inplace=True)
            
            if kind == 'eta_sto_in':
                self.eta_sto_in = eta
                return self.eta_sto_in
            elif kind == 'eta_sto_out':
                self.eta_sto_out = eta
                return self.eta_sto_out
            elif kind == 'eta_sto':
                self.eta_sto_roundtrip = eta
                return self.eta_sto_roundtrip
        except:
            raise
            print('E:getStorageEfficiency')
        

    def getStorageCycling(self, kind='number'):
        '''
        Finds contemporaneous storage charging and discharging for all
        scenarios.
        
        Possible unintended storage cycling cases:
            - case1 (neutral): storage charge = storage discharge -> same & across period cycling
            - case2 (char): storage charge < storage discharge -> same & across period cycling
            - case3 (dis): storage charge > storage discharge, discharge > cycled Energy from same period -> same & across period cycling
            - case4 (dis_same): storage charge > storage discharge, discharge <= available cycling energy potential (as per charge) from same period -> same period cycling only
        
        The sum of all cases does not have a suffix.
        
        Possible configurations:
            TODO: 
            kind='list': list of all hour indices where storage cycling occurs [hour indices]
            kind='list_discharge': list of all hour indices where discharge storage cycling occurs [hour indices]
            kind='list_charge': list of all hour indices where charge storage cycling occurs [hour indices]
            kind='list_neutral': list of all hour indices where neutral storage cycling occurs [hour indices]
            kind='number': total number of hours where storage cycling occurs [total number]
            kind='number_discharge': total number of hours where discharge storage cycling occurs [total number]
            kind='number_charge': total number of hours where charge storage cycling occurs [total number]
            kind='number_neutral': total number of hours where neutral storage cycling occurs [total number]
            kind='energy_overview': overview of all aggregated indices indicating total energy amount [TWh]
            kind='spc_total': total energy amount of same-period-cycling [TWh]
            kind='spc_discharging': total energy amount of same-period-cycling from discharge storage cycling [TWh]
            kind='spc_charging': total energy amount of same-period-cycling from charge storage cycling [TWh]
            kind='spc_neutral': total energy amount of same-period-cycling from neutral storage cycling [TWh]
            kind='unduly_cycled_energy_total': total unduly cycled energy amount [TWh]
            kind='unduly_cycled_energy_discharge': total unduly cycled energy amount from discharge storage cycling [TWh]
            kind='unduly_cycled_energy_charge': total unduly cycled energy amount from charge storage cycling [TWh]
            kind='unduly_cycled_energy_neutral': total unduly cycled energy amount from neutral storage cycling [TWh]
            kind='loss_total': total conversion loss due to storage cycling [TWh]
            kind='loss_total_from_discharge_storage_cycling': total conversion loss due to discharge storage cycling [TWh]
            kind='loss_total_from_charge_storage_cycling': total conversion loss due to charge storage cycling [TWh]
            kind='loss_total_from_neutral_storage_cycling': total conversion loss due to neutral storage cycling [TWh]
            # kind='relative': share in total demand [%]


        Finds all scenarios w/o storage cycling and saves them to the attribute
        'no_storage_cycling'.

        Returns
        -------
        None.

        '''
        try:
            # if results are not already calculated start calculation
            if not hasattr(self, 'sto_cyc_energy'):
                
                # begin calculations
                if not hasattr(self, 'storage_hourly_gen'):
                    sto_gen_hourly = self.getStorageGeneration('hourly')
                else:
                    sto_gen_hourly = self.storage_hourly_gen.copy()
                
                sto_gen_hourly.replace(np.nan, 0, inplace=True)
                sto_gen_hourly.drop(sto_gen_hourly.columns.difference(['STO_IN.l [MW]', 'scenario', 'STO_OUT.l [MW]']), 
                                    axis=1, inplace=True)
                
                # group scenarios
                sto_gen_hourly_scen = {}
                for scen, group in sto_gen_hourly.groupby('scenario'):
                    df = group.sort_index()
                    df.drop(df.columns.difference(['STO_IN.l [MW]', 'STO_OUT.l [MW]']),
                            axis=1, inplace=True)
                    sto_gen_hourly_scen[scen] = df
                
                eta_sto_in = self.getStorageEfficiency(kind='eta_sto_in')
                eta_sto_out = self.getStorageEfficiency(kind='eta_sto_out')
                
                # data container
                sto_cyc_h = {}
                sto_cyc_gen = {}
                sto_cyc_neutral_h = {}
                sto_cyc_neutral_gen = {}
                sto_cyc_char_h = {}
                sto_cyc_char_gen = {}
                sto_cyc_dis_h = {}
                sto_cyc_dis_gen = {}
                sto_cyc_dis_same_h = {}
                sto_cyc_dis_same_gen = {}
                
                # filter hours with storage cycling (all, neutral, char, dis, dis_same)
                for scen, df in sto_gen_hourly_scen.items():
                    
                    # get conversion efficiecies
                    eta_in_temp = eta_sto_in.loc[scen, 'eta_sto_in [%]']
                    eta_out_temp = eta_sto_out.loc[scen, 'eta_sto_out [%]']
                    
                    # all storage cycling occurrences
                    mask = (df['STO_IN.l [MW]'] > 0) & (df['STO_OUT.l [MW]'] > 0)
                    sto_cyc_h[scen] = mask[mask].index.values.astype(int).tolist()
                    sto_cyc_gen[scen] = df[mask]
                    
                    # neutral storage cycling (storage charge = discharge)
                    mask_neutral = (df['STO_IN.l [MW]'] > 0) & (df['STO_OUT.l [MW]'] > 0) & (df['STO_IN.l [MW]'] == df['STO_OUT.l [MW]'])
                    sto_cyc_neutral_h[scen] = mask_neutral[mask_neutral].index.values.astype(int).tolist()
                    sto_cyc_neutral_gen[scen] = df[mask_neutral]
                    
                    # charge storage cycling (storage charge < discharge)
                    mask_char = (df['STO_IN.l [MW]'] > 0) & (df['STO_OUT.l [MW]'] > 0) & (df['STO_IN.l [MW]'] < df['STO_OUT.l [MW]'])
                    sto_cyc_char_h[scen] = mask_char[mask_char].index.values.astype(int).tolist()
                    sto_cyc_char_gen[scen] = df[mask_char]
                    
                    # discharge storage cycling (storage charge > discharge, discharge > cycled Energy from same period)
                    mask_dis = (df['STO_IN.l [MW]'] > 0) & (df['STO_OUT.l [MW]'] > 0) & \
                               (df['STO_IN.l [MW]'] > df['STO_OUT.l [MW]']) & \
                               (df['STO_OUT.l [MW]'] > ( df['STO_IN.l [MW]'] * eta_in_temp * eta_out_temp) )
                    sto_cyc_dis_h[scen] = mask_dis[mask_dis].index.values.astype(int).tolist()
                    sto_cyc_dis_gen[scen] = df[mask_dis]
                    
                    # discharge storage cycling (storage charge > discharge, discharge <= cycled Energy from same period)
                    mask_dis_same = (df['STO_IN.l [MW]'] > 0) & (df['STO_OUT.l [MW]'] > 0) & \
                                    (df['STO_IN.l [MW]'] > df['STO_OUT.l [MW]']) & \
                                    (df['STO_OUT.l [MW]'] <= ( df['STO_IN.l [MW]'] * eta_in_temp * eta_out_temp) )
                    sto_cyc_dis_same_h[scen] = mask_dis_same[mask_dis_same].index.values.astype(int).tolist()
                    sto_cyc_dis_same_gen[scen] = df[mask_dis_same]
                
                # compute SPC, PPC, and its components for neutral storage cycling (charge > discharge)
                for scen, df in sto_cyc_neutral_gen.items():
                    
                    # get conversion efficiecies
                    eta_in_temp = eta_sto_in.loc[scen, 'eta_sto_in [%]']
                    eta_out_temp = eta_sto_out.loc[scen, 'eta_sto_out [%]']
                    
                    # compute indices
                    df = df.copy()
                    df['SPC [MW]'] = df['STO_IN.l [MW]'] 
                    df['Unintendedly cycled energy (SPC) [MW]'] = df['STO_IN.l [MW]'] * (eta_in_temp * eta_out_temp)
                    df['Loss charging (SPC) [MW]'] = df['STO_IN.l [MW]'] * (1 - eta_in_temp)
                    df['Loss discharging (SPC) [MW]'] = df['STO_IN.l [MW]'] * (eta_in_temp - eta_in_temp * eta_out_temp)
                    df['Unintendedly cycled energy (PPC) [MW]'] = df['STO_IN.l [MW]'] - df['Unintendedly cycled energy (SPC) [MW]']
                    df['Loss charging (PPC) [MW]'] = (df['Unintendedly cycled energy (PPC) [MW]'] / eta_out_temp) * (1 / eta_in_temp - 1)
                    df['Loss discharging (PPC) [MW]'] = df['Unintendedly cycled energy (PPC) [MW]'] * (1 / eta_out_temp - 1)
                    df['PPC [MW]'] = df['Unintendedly cycled energy (PPC) [MW]'] + df['Loss charging (PPC) [MW]'] + df['Loss discharging (PPC) [MW]'] 
                    sto_cyc_neutral_gen[scen] = df
                
                # compute SPC, PPC, and its components for charging storage cycling (charge < discharge)
                for scen, df in sto_cyc_char_gen.items():
                    
                    # get conversion efficiecies
                    eta_in_temp = eta_sto_in.loc[scen, 'eta_sto_in [%]']
                    eta_out_temp = eta_sto_out.loc[scen, 'eta_sto_out [%]']
                    
                    # compute indices
                    df = df.copy()
                    df['SPC [MW]'] = df['STO_IN.l [MW]'] 
                    df['Unintendedly cycled energy (SPC) [MW]'] = df['STO_IN.l [MW]'] * (eta_in_temp * eta_out_temp)
                    df['Loss charging (SPC) [MW]'] = df['STO_IN.l [MW]'] * (1 - eta_in_temp)
                    df['Loss discharging (SPC) [MW]'] = df['STO_IN.l [MW]'] * (eta_in_temp - eta_in_temp * eta_out_temp)
                    df['Unintendedly cycled energy (PPC) [MW]'] = df['STO_IN.l [MW]'] - df['Unintendedly cycled energy (SPC) [MW]']
                    df['Loss charging (PPC) [MW]'] = (df['Unintendedly cycled energy (PPC) [MW]'] / eta_out_temp) * (1 / eta_in_temp - 1)
                    df['Loss discharging (PPC) [MW]'] = df['Unintendedly cycled energy (PPC) [MW]'] * (1 / eta_out_temp - 1)
                    df['PPC [MW]'] = df['Unintendedly cycled energy (PPC) [MW]'] + df['Loss charging (PPC) [MW]'] + df['Loss discharging (PPC) [MW]'] 
                    df['Intended discharge (PPC) [MW]'] = df['STO_OUT.l [MW]'] - df['STO_IN.l [MW]']
                    sto_cyc_char_gen[scen] = df
                
                # compute SPC, PPC, and its components for discharging storage cycling (charge > discharge, discharge > cycled Energy from same period)
                for scen, df in sto_cyc_dis_gen.items():
                    
                    # get conversion efficiecies
                    eta_in_temp = eta_sto_in.loc[scen, 'eta_sto_in [%]']
                    eta_out_temp = eta_sto_out.loc[scen, 'eta_sto_out [%]']
                    
                    # compute indices
                    df = df.copy()
                    df['SPC [MW]'] = df['STO_IN.l [MW]'] 
                    df['Unintendedly cycled energy (SPC) [MW]'] = df['STO_IN.l [MW]'] * (eta_in_temp * eta_out_temp)
                    df['Loss charging (SPC) [MW]'] = df['STO_IN.l [MW]'] * (1 - eta_in_temp)
                    df['Loss discharging (SPC) [MW]'] = df['STO_IN.l [MW]'] * (eta_in_temp - eta_in_temp * eta_out_temp)
                    df['Unintendedly cycled energy (PPC) [MW]'] = df['STO_OUT.l [MW]'] - df['Unintendedly cycled energy (SPC) [MW]']
                    df['Loss charging (PPC) [MW]'] = (df['Unintendedly cycled energy (PPC) [MW]'] / eta_out_temp) * (1 / eta_in_temp - 1)
                    df['Loss discharging (PPC) [MW]'] = df['Unintendedly cycled energy (PPC) [MW]'] * (1 / eta_out_temp - 1)
                    df['PPC [MW]'] = df['Unintendedly cycled energy (PPC) [MW]'] + df['Loss charging (PPC) [MW]'] + df['Loss discharging (PPC) [MW]']
                    sto_cyc_dis_gen[scen] = df
                
                # compute SPC, PPC, and its components for discharging storage cycling same period only (charge > discharge, discharge <= available cycling energy potential (as per charge) from same period)
                for scen, df in sto_cyc_dis_same_gen.items():
                    
                    # get conversion efficiecies
                    eta_in_temp = eta_sto_in.loc[scen, 'eta_sto_in [%]']
                    eta_out_temp = eta_sto_out.loc[scen, 'eta_sto_out [%]']
                    
                    # compute indices
                    df = df.copy()
                    df['Unintendedly cycled energy (SPC) [MW]'] = df['STO_OUT.l [MW]']
                    df['Loss charging (SPC) [MW]'] = (df['Unintendedly cycled energy (SPC) [MW]'] / eta_out_temp) * (1 / eta_in_temp - 1)
                    df['Loss discharging (SPC) [MW]'] = df['Unintendedly cycled energy (SPC) [MW]'] * (1 / eta_out_temp - 1)
                    df['SPC [MW]'] = df['Unintendedly cycled energy (SPC) [MW]'] + df['Loss charging (SPC) [MW]'] + df['Loss discharging (SPC) [MW]']
                    df['Intended charge (SPC) [MW]'] = df['STO_IN.l [MW]'] - df['SPC [MW]']
                    sto_cyc_dis_same_gen[scen] = df
                    
                # data container
                sto_cyc_energy = pd.DataFrame(columns=['SPC total [TWh]', 
                                                       'SPC neutral [TWh]',
                                                       'SPC char [TWh]',
                                                       'SPC dis [TWh]',
                                                       'SPC dis_same [TWh]',
                                                       'Unintendedly cycled energy (SPC) total [TWh]',
                                                       'Unintendedly cycled energy (SPC) neutral [TWh]',
                                                       'Unintendedly cycled energy (SPC) char [TWh]',
                                                       'Unintendedly cycled energy (SPC) dis [TWh]',
                                                       'Unintendedly cycled energy (SPC) dis_same [TWh]',
                                                       'Loss charging (SPC) total [TWh]',
                                                       'Loss charging (SPC) neutral [TWh]',
                                                       'Loss charging (SPC) char [TWh]',
                                                       'Loss charging (SPC) dis [TWh]',
                                                       'Loss charging (SPC) dis_same [TWh]',
                                                       'Loss discharging (SPC) total [TWh]',
                                                       'Loss discharging (SPC) neutral [TWh]',
                                                       'Loss discharging (SPC) char [TWh]',
                                                       'Loss discharging (SPC) dis [TWh]',
                                                       'Loss discharging (SPC) dis_same [TWh]',
                                                       'Loss (SPC) total [TWh]',
                                                       'Loss (SPC) neutral total [TWh]',
                                                       'Loss (SPC) char total [TWh]',
                                                       'Loss (SPC) dis total [TWh]',
                                                       'Loss (SPC) dis_same total [TWh]',
                                                       'PPC total [TWh]',
                                                       'PPC neutral [TWh]',
                                                       'PPC char [TWh]',
                                                       'PPC dis [TWh]',
                                                       'Unintendedly cycled energy (PPC) total [TWh]',
                                                       'Unintendedly cycled energy (PPC) neutral [TWh]',
                                                       'Unintendedly cycled energy (PPC) char [TWh]',
                                                       'Unintendedly cycled energy (PPC) dis [TWh]',
                                                       'Loss charging (PPC) total [TWh]',
                                                       'Loss charging (PPC) neutral [TWh]',
                                                       'Loss charging (PPC) char [TWh]',
                                                       'Loss charging (PPC) dis [TWh]',
                                                       'Loss discharging (PPC) total [TWh]',
                                                       'Loss discharging (PPC) neutral [TWh]',
                                                       'Loss discharging (PPC) char [TWh]',
                                                       'Loss discharging (PPC) dis [TWh]',
                                                       'Loss (PPC) total [TWh]',
                                                       'Loss (PPC) neutral total [TWh]',
                                                       'Loss (PPC) char total [TWh]',
                                                       'Loss (PPC) dis total [TWh]',
                                                       'Loss (SPC+PPC) total [TWh]'],
                                              index=self.scen_list)
                
                sto_cyc_energy.sort_index(inplace=True, ascending=False)
                
                # sum total energy amount for neutral storage cycling
                for scen, df in sto_cyc_neutral_gen.items():
                    sto_cyc_energy.loc[scen, 'SPC neutral [TWh]'] = df['SPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (SPC) neutral [TWh]'] = df['Unintendedly cycled energy (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (SPC) neutral [TWh]'] = df['Loss charging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (SPC) neutral [TWh]'] = df['Loss discharging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'PPC neutral [TWh]'] = df['PPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (PPC) neutral [TWh]'] = df['Unintendedly cycled energy (PPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (PPC) neutral [TWh]'] = df['Loss charging (PPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (PPC) neutral [TWh]'] = df['Loss discharging (PPC) [MW]'].sum() / 1e6
                
                # sum total energy amount for charge storage cycling
                for scen, df in sto_cyc_char_gen.items():
                    sto_cyc_energy.loc[scen, 'SPC char [TWh]'] = df['SPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (SPC) char [TWh]'] = df['Unintendedly cycled energy (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (SPC) char [TWh]'] = df['Loss charging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (SPC) char [TWh]'] = df['Loss discharging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'PPC char [TWh]'] = df['PPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (PPC) char [TWh]'] = df['Unintendedly cycled energy (PPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (PPC) char [TWh]'] = df['Loss charging (PPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (PPC) char [TWh]'] = df['Loss discharging (PPC) [MW]'].sum() / 1e6
                    
                # sum total energy amount for discharge storage cycling
                for scen, df in sto_cyc_dis_gen.items():
                    sto_cyc_energy.loc[scen, 'SPC dis [TWh]'] = df['SPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (SPC) dis [TWh]'] = df['Unintendedly cycled energy (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (SPC) dis [TWh]'] = df['Loss charging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (SPC) dis [TWh]'] = df['Loss discharging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'PPC dis [TWh]'] = df['PPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (PPC) dis [TWh]'] = df['Unintendedly cycled energy (PPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (PPC) dis [TWh]'] = df['Loss charging (PPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (PPC) dis [TWh]'] = df['Loss discharging (PPC) [MW]'].sum() / 1e6
                
                # sum total energy amount for discharge storage cycling same period only
                for scen, df in sto_cyc_dis_same_gen.items():
                    sto_cyc_energy.loc[scen, 'SPC dis_same [TWh]'] = df['SPC [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Unintendedly cycled energy (SPC) dis_same [TWh]'] = df['Unintendedly cycled energy (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss charging (SPC) dis_same [TWh]'] = df['Loss charging (SPC) [MW]'].sum() / 1e6
                    sto_cyc_energy.loc[scen, 'Loss discharging (SPC) dis_same [TWh]'] = df['Loss discharging (SPC) [MW]'].sum() / 1e6
                
                # sum total energy amount
                sto_cyc_energy['SPC total [TWh]'] = sto_cyc_energy['SPC neutral [TWh]'] + \
                                                    sto_cyc_energy['SPC char [TWh]'] + \
                                                    sto_cyc_energy['SPC dis [TWh]'] + \
                                                    sto_cyc_energy['SPC dis_same [TWh]']
                                                    
                sto_cyc_energy['Unintendedly cycled energy (SPC) total [TWh]'] = sto_cyc_energy['Unintendedly cycled energy (SPC) neutral [TWh]'] +\
                                                                                 sto_cyc_energy['Unintendedly cycled energy (SPC) char [TWh]'] +\
                                                                                 sto_cyc_energy['Unintendedly cycled energy (SPC) dis [TWh]'] +\
                                                                                 sto_cyc_energy['Unintendedly cycled energy (SPC) dis_same [TWh]']
                                                                                 
                sto_cyc_energy['Loss charging (SPC) total [TWh]'] = sto_cyc_energy['Loss charging (SPC) neutral [TWh]'] +\
                                                                    sto_cyc_energy['Loss charging (SPC) char [TWh]'] +\
                                                                    sto_cyc_energy['Loss charging (SPC) dis [TWh]'] +\
                                                                    sto_cyc_energy['Loss charging (SPC) dis_same [TWh]']
                                                                    
                sto_cyc_energy['Loss discharging (SPC) total [TWh]'] = sto_cyc_energy['Loss discharging (SPC) neutral [TWh]'] +\
                                                                       sto_cyc_energy['Loss discharging (SPC) char [TWh]'] +\
                                                                       sto_cyc_energy['Loss discharging (SPC) dis [TWh]'] +\
                                                                       sto_cyc_energy['Loss discharging (SPC) dis_same [TWh]']
                
                sto_cyc_energy['Loss (SPC) total [TWh]'] = sto_cyc_energy['Loss charging (SPC) total [TWh]'] +\
                                                           sto_cyc_energy['Loss discharging (SPC) total [TWh]']
                
                sto_cyc_energy['Loss (SPC) neutral total [TWh]'] = sto_cyc_energy['Loss charging (SPC) neutral [TWh]'] +\
                                                                   sto_cyc_energy['Loss discharging (SPC) neutral [TWh]']
                                                                   
                sto_cyc_energy['Loss (SPC) char total [TWh]'] = sto_cyc_energy['Loss charging (SPC) char [TWh]'] +\
                                                                sto_cyc_energy['Loss discharging (SPC) char [TWh]']
                
                sto_cyc_energy['Loss (SPC) dis total [TWh]'] = sto_cyc_energy['Loss charging (SPC) dis [TWh]'] +\
                                                               sto_cyc_energy['Loss discharging (SPC) dis [TWh]']
                
                sto_cyc_energy['Loss (SPC) dis_same total [TWh]'] = sto_cyc_energy['Loss charging (SPC) dis_same [TWh]'] +\
                                                                    sto_cyc_energy['Loss discharging (SPC) dis_same [TWh]']
               
                sto_cyc_energy['PPC total [TWh]'] = sto_cyc_energy['PPC neutral [TWh]'] + \
                                                    sto_cyc_energy['PPC char [TWh]'] + \
                                                    sto_cyc_energy['PPC dis [TWh]']
               
                sto_cyc_energy['Unintendedly cycled energy (PPC) total [TWh]'] = sto_cyc_energy['Unintendedly cycled energy (PPC) neutral [TWh]'] +\
                                                                                 sto_cyc_energy['Unintendedly cycled energy (PPC) char [TWh]'] +\
                                                                                 sto_cyc_energy['Unintendedly cycled energy (PPC) dis [TWh]'] 
               
                sto_cyc_energy['Loss charging (PPC) total [TWh]'] = sto_cyc_energy['Loss charging (PPC) neutral [TWh]'] +\
                                                                    sto_cyc_energy['Loss charging (PPC) char [TWh]'] +\
                                                                    sto_cyc_energy['Loss charging (PPC) dis [TWh]']
                
                sto_cyc_energy['Loss discharging (PPC) total [TWh]'] = sto_cyc_energy['Loss discharging (PPC) neutral [TWh]'] +\
                                                                       sto_cyc_energy['Loss discharging (PPC) char [TWh]'] +\
                                                                       sto_cyc_energy['Loss discharging (PPC) dis [TWh]']                                                                    
               
                sto_cyc_energy['Loss (PPC) total [TWh]'] = sto_cyc_energy['Loss charging (PPC) total [TWh]'] +\
                                                           sto_cyc_energy['Loss discharging (PPC) total [TWh]']
                
                sto_cyc_energy['Loss (PPC) neutral total [TWh]'] = sto_cyc_energy['Loss charging (PPC) neutral [TWh]'] +\
                                                                   sto_cyc_energy['Loss discharging (PPC) neutral [TWh]']
                                                                   
                sto_cyc_energy['Loss (PPC) char total [TWh]'] = sto_cyc_energy['Loss charging (PPC) char [TWh]'] +\
                                                                sto_cyc_energy['Loss discharging (PPC) char [TWh]']
                
                sto_cyc_energy['Loss (PPC) dis total [TWh]'] = sto_cyc_energy['Loss charging (PPC) dis [TWh]'] +\
                                                               sto_cyc_energy['Loss discharging (PPC) dis [TWh]']
               
                sto_cyc_energy['Loss (SPC+PPC) total [TWh]'] = sto_cyc_energy['Loss (SPC) total [TWh]'] +\
                                                               sto_cyc_energy['Loss (PPC) total [TWh]']
                
                sto_cyc_energy.fillna(0, inplace=True)
        
                # add scenarios if missing
                for scen in self.scen_list:
                    if scen not in sto_gen_hourly_scen.keys():
                        sto_gen_hourly_scen[scen] = []
                    if scen not in sto_cyc_h.keys():
                        sto_cyc_h[scen] = []
                    if scen not in sto_cyc_neutral_h.keys():
                        sto_cyc_neutral_h[scen] = []
                    if scen not in sto_cyc_char_h.keys():
                        sto_cyc_char_h[scen] = []
                    if scen not in sto_cyc_dis_h.keys():
                        sto_cyc_dis_h[scen] = []
                    if scen not in sto_cyc_dis_same_h.keys():
                        sto_cyc_dis_same_h[scen] = []
                    if scen not in sto_cyc_gen.keys():
                        sto_cyc_gen[scen] = []
                    if scen not in sto_cyc_neutral_gen.keys():
                        sto_cyc_neutral_gen[scen] = []
                    if scen not in sto_cyc_char_gen.keys():
                        sto_cyc_char_gen[scen] = []
                    if scen not in sto_cyc_dis_gen.keys():
                        sto_cyc_dis_gen[scen] = []
                    if scen not in sto_cyc_dis_same_gen.keys():
                        sto_cyc_dis_same_gen[scen] = []
                    
                # transform to dataframe in matrix form and assign to object
                self.sto_cyc_list, self.sto_cyc_number = self.indexListNumber2Matrix(sto_cyc_h)
                self.sto_cyc_neutral_list, self.sto_cyc_neutral_number = self.indexListNumber2Matrix(sto_cyc_neutral_h)
                self.sto_cyc_char_list, self.sto_cyc_char_number = self.indexListNumber2Matrix(sto_cyc_char_h)
                self.sto_cyc_dis_list, self.sto_cyc_dis_number = self.indexListNumber2Matrix(sto_cyc_dis_h)
                self.sto_cyc_dis_same_list, self.sto_cyc_dis_same_number = self.indexListNumber2Matrix(sto_cyc_dis_same_h)
                
                # transform to dataframe in matrix form and assign to object
                self.sto_cyc_spc_total = self.resultList2Matrix(sto_cyc_energy, col_name='SPC total [TWh]')
                self.sto_cyc_spc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='SPC neutral [TWh]')
                self.sto_cyc_spc_char = self.resultList2Matrix(sto_cyc_energy, col_name='SPC char [TWh]')
                self.sto_cyc_spc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='SPC dis [TWh]')
                self.sto_cyc_spc_dis_same = self.resultList2Matrix(sto_cyc_energy, col_name='SPC dis_same [TWh]')
                self.sto_cyc_unint_cyc_e_spc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (SPC) total [TWh]')
                self.sto_cyc_unint_cyc_e_spc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (SPC) neutral [TWh]')
                self.sto_cyc_unint_cyc_e_spc_char = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (SPC) char [TWh]')
                self.sto_cyc_unint_cyc_e_spc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (SPC) dis [TWh]')
                self.sto_cyc_unint_cyc_e_spc_dis_same = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (SPC) dis_same [TWh]')
                self.sto_cyc_loss_charging_spc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (SPC) total [TWh]')
                self.sto_cyc_loss_charging_spc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (SPC) neutral [TWh]')
                self.sto_cyc_loss_charging_spc_char = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (SPC) char [TWh]')
                self.sto_cyc_loss_charging_spc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (SPC) dis [TWh]')
                self.sto_cyc_loss_charging_spc_dis_same = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (SPC) dis_same [TWh]')
                self.sto_cyc_loss_discharging_spc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (SPC) total [TWh]')
                self.sto_cyc_loss_discharging_spc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (SPC) neutral [TWh]')
                self.sto_cyc_loss_discharging_spc_char = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (SPC) char [TWh]')
                self.sto_cyc_loss_discharging_spc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (SPC) dis [TWh]')
                self.sto_cyc_loss_discharging_spc_dis_same = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (SPC) dis_same [TWh]')
                self.sto_cyc_loss_spc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (SPC) total [TWh]')
                self.sto_cyc_loss_spc_neutral_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (SPC) neutral total [TWh]')
                self.sto_cyc_loss_spc_char_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (SPC) char total [TWh]')
                self.sto_cyc_loss_spc_dis_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (SPC) dis total [TWh]')
                self.sto_cyc_loss_spc_dis_same_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (SPC) dis_same total [TWh]')
                self.sto_cyc_ppc_total = self.resultList2Matrix(sto_cyc_energy, col_name='PPC total [TWh]')
                self.sto_cyc_ppc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='PPC neutral [TWh]')
                self.sto_cyc_ppc_char = self.resultList2Matrix(sto_cyc_energy, col_name='PPC char [TWh]')
                self.sto_cyc_ppc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='PPC dis [TWh]')
                self.sto_cyc_unint_cyc_e_ppc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (PPC) total [TWh]')
                self.sto_cyc_unint_cyc_e_ppc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (PPC) neutral [TWh]')
                self.sto_cyc_unint_cyc_e_ppc_char = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (PPC) char [TWh]')
                self.sto_cyc_unint_cyc_e_ppc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='Unintendedly cycled energy (PPC) dis [TWh]')
                self.sto_cyc_loss_charging_ppc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (PPC) total [TWh]')
                self.sto_cyc_loss_charging_ppc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (PPC) neutral [TWh]')
                self.sto_cyc_loss_charging_ppc_char = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (PPC) char [TWh]')
                self.sto_cyc_loss_charging_ppc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='Loss charging (PPC) dis [TWh]')
                self.sto_cyc_loss_discharging_ppc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (PPC) total [TWh]')
                self.sto_cyc_loss_discharging_ppc_neutral = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (PPC) neutral [TWh]')
                self.sto_cyc_loss_discharging_ppc_char = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (PPC) char [TWh]')
                self.sto_cyc_loss_discharging_ppc_dis = self.resultList2Matrix(sto_cyc_energy, col_name='Loss discharging (PPC) dis [TWh]')
                self.sto_cyc_loss_ppc_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (PPC) total [TWh]')
                self.sto_cyc_loss_ppc_neutral_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (PPC) neutral total [TWh]')
                self.sto_cyc_loss_ppc_char_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (PPC) char total [TWh]')
                self.sto_cyc_loss_ppc_dis_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (PPC) dis total [TWh]')
                self.sto_cyc_loss_total = self.resultList2Matrix(sto_cyc_energy, col_name='Loss (SPC+PPC) total [TWh]')
    
                # find all constraints without storage cycling
                cons_no_sto_cyc = []
                for con in self.sto_cyc_number:
                    if (self.sto_cyc_number[con] == 0).all():
                        cons_no_sto_cyc.append(con)
                self.cons_no_sto_cyc = cons_no_sto_cyc
                        
                # find all scenarios without storage cycling
                # TODO
    
                # assign overview dataframe to attributes
                self.sto_cyc_energy = sto_cyc_energy

            # TODO: return results
            if kind == 'list':
                return self.sto_cyc_list
            elif kind == 'list_discharge':
                return self.sto_cyc_dis_list
            elif kind == 'list_charge':
                return self.sto_cyc_char_list
            elif kind == 'list_neutral':
                return self.sto_cyc_neutral_list
            elif kind == 'number':
                return self.sto_cyc_number
            elif kind == 'number_discharge':
                return self.sto_cyc_dis_number
            elif kind == 'number_charge':
                return self.sto_cyc_char_number
            elif kind == 'number_neutral':
                return self.sto_cyc_neutral_number
            elif kind == 'energy_overview':
                return self.sto_cyc_energy
            elif kind == 'spc_total':
                return self.sto_cyc_spc_total
            elif kind == 'ppc_total':
                return self.sto_cyc_ppc_total
            elif kind == 'loss_total':
                return self.sto_cyc_loss_total
            elif kind == 'spc_cyc_e':
                return self.sto_cyc_unint_cyc_e_spc_total
            elif kind == 'ppc_cyc_e':
                return self.sto_cyc_unint_cyc_e_ppc_total
            elif kind == 'spc_loss_total':
                return self.sto_cyc_loss_spc_total
            elif kind == 'ppc_loss_total':
                return self.sto_cyc_loss_ppc_total
            
# =============================================================================
#             elif kind == 'relative':
#                 if not hasattr(self, 'demand_total'):
#                     demand_total = self.getDemand('total')
#                 else:
#                     demand_total = self.demand_total.copy()
#                 storage_cycling_relative = (storage_cycling_energy / demand_total) * 100
#                 self.storage_cycling_relative = storage_cycling_relative
#                 return self.storage_cycling_relative
# =============================================================================
        except:
            raise
            print('E:getStorageCycling')
            
    def getStorageChargingConventionals(self, kind='number'):
        '''
        Returns results on conventionals charging storages. 
        
        Possible configurations:
            kind='number': number of hours [total number]
            kind='list': list of hour indices [hour indices]
            kind='energy': total annual amount of energy [TWh]

        Parameters
        ----------
        kind : TYPE, optional
            DESCRIPTION. The default is 'number'.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if not hasattr(self, 'sto_charge_hourly'):
                sto_hourly_charge = self.getStorageCharge('hourly')
            else:
                sto_hourly_charge = self.sto_charge_hourly.copy()
            if not hasattr(self, 'res_gen_hourly'):
                res_gen_hourly = self.getRESGeneration('hourly')
            else:
                res_gen_hourly = self.res_gen_hourly.copy()
            
            for scen, df in sto_hourly_charge.items():
                sto_hourly_charge[scen] = pd.concat([df, res_gen_hourly['pv'][scen], res_gen_hourly['wind'][scen]], axis=1)
                        
            sto_char_conv_h_dc = {}
            sto_char_conv_e_dc = {}
            for scen, df in sto_hourly_charge.items():
                mask = (df['STO_IN.l [MW]'] > (df['G_pv.l [MW]'] + df['G_wind.l [MW]']))
                sto_char_conv_h_dc[scen] = mask[mask].index.values.astype(int).tolist()
                sto_char_conv_e_dc[scen] = df[mask]

            # sum charged energy from conventionals
            for scen, df in sto_char_conv_e_dc.items():
                sto_char_conv_e_dc[scen]['Conventional_Charge [MW]'] = df['STO_IN.l [MW]'] - (df['G_pv.l [MW]'] + df['G_wind.l [MW]'])
            
            sto_char_conv_e_sum_dc = {}
            for scen, df in sto_char_conv_e_dc.items():
                sto_char_conv_e_sum_dc[scen] = df['Conventional_Charge [MW]'].sum()

            # add all scenarios to sto_char_conv_h_dc
            for scen in self.scen_list:
                if scen not in sto_char_conv_h_dc.keys():
                    sto_char_conv_h_dc[scen] = []
                if scen not in sto_char_conv_e_dc.keys():
                    sto_char_conv_e_dc[scen] = []
                if scen not in sto_char_conv_e_sum_dc.keys():
                    sto_char_conv_e_sum_dc[scen] = 0
            
            # transform to dataframe
            sto_char_conv_list, sto_char_conv_number = self.indexListNumber2Matrix(sto_char_conv_h_dc)
            sto_char_conv_energy= self.indexListNumber2Matrix(sto_char_conv_e_sum_dc, only_list=True)
        
            # adjust energy unit from MWh to TWh
            sto_char_conv_energy /= 1e6
            
            # find all constraints without storage cycling
            no_sto_char_conv = []
            for con in sto_char_conv_number:
                if (sto_char_conv_number[con] == 0).all():
                    sto_char_conv_number.append(con)

            # assign attributes
            self.cons_no_sto_char_conv = no_sto_char_conv
            self.sto_char_conv_list = sto_char_conv_list
            self.sto_char_conv_number = sto_char_conv_number
            self.sto_char_conv_energy = sto_char_conv_energy
            self.sto_char_conv_energy_hourly = sto_char_conv_e_dc

            if kind == 'number':
                return self.sto_char_conv_number
            elif kind == 'list':
                return self.sto_char_conv_list
            elif kind == 'energy':
                return self.sto_char_conv_energy
            elif kind == 'df':
                return self.sto_char_conv_energy_hourly
            
        except:
            raise
            print('E:getStorageChargingConventionals')

    def getRESCurtailment(self, kind='total_absolute'):
        '''
        Unit for kind='hourly': MWh
        Unit for kind='total_absolute': TWh
        Unit for kind='total_relative': % share in interval [0, 100]

        Parameters
        ----------
        kind : TYPE, optional
            DESCRIPTION. The default is 'total_absolute'.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        try:
            if kind == 'hourly':
                df = self.model_results.copy()
                df = df[df['Symbol'] == 'CU']
                df.set_index('h', inplace=True)
                df.index = df.index.astype('int')
                df.rename(columns={'res': 'tech', 'Level': 'CU.l [MW]', 'Marginal': 'CU.m [EUR/MW]'},
                          inplace=True)
                df.drop(df.columns.difference(['tech', 'CU.l [MW]', 'CU.m [EUR/MW]',
                                               'scenario']),
                        axis=1, inplace=True)
                
                # group according to tech
                curt_hourly = {}
                for tech, group in df.groupby('tech'):
                     df = group[['CU.l [MW]', 'CU.m [EUR/MW]', 'scenario']]
                     curt_hourly[tech] = {scen: group for scen, group in df.groupby('scenario')}
                
                # add hours with availability factor of one
                full_year = pd.DataFrame(index=[i for i in range(1, 8761)], columns=['temp'], dtype='float64')
                zeros = [0 for i in range(1,8761)]
                full_year_zeros = pd.Series(index=[i for i in range(1, 8761)], name='CU.l [MW]', data=zeros)
                
                for tech, data_dc in curt_hourly.items():
                    for scen, df in data_dc.items():
                        _df = pd.concat([df, full_year], axis=1)
                        _df.fillna(0, inplace=True)
                        _df = _df['CU.l [MW]']
                        _df.rename(str('CU_' + tech + '.l [MW]'), inplace=True)
                        curt_hourly[tech][scen] = _df
            
                # add scenarios if missing
                for scen in self.scen_list:
                    if scen not in curt_hourly[tech].keys():
                        curt_hourly[tech][scen] = full_year_zeros    
            
                self.curt_hourly = curt_hourly
                return self.curt_hourly

            elif kind == 'total_absolute':
                if not hasattr(self, 'curt_hourly'):
                    curt_hourly = self.getRESCurtailment('hourly')
                else:
                    curt_hourly = self.curt_hourly.copy()
                
                # aggregate hourly values to annual energy amount
                curt_total_abs = {}
                for tech, data_dc in curt_hourly.items():
                    curt_total_abs[tech] = {}
                    for scen, s in data_dc.items():
                        curt_total_abs[tech][scen] = s.sum()
                
                # convert to DataFrame and from MW to TWh
                for tech, data_dc in curt_total_abs.items():
                    df_temp = pd.DataFrame.from_dict(data_dc, orient='index', columns=['CU.l [TWh]'])
                    curt_total_abs[tech] = df_temp / 1e6
                    
                # convert to df
                for tech, df in curt_total_abs.items():
                    curt_total_abs[tech] = self.resultList2Matrix(df, col_idx=0)
                        
                self.curt_total_abs = curt_total_abs
                return self.curt_total_abs

            elif kind == 'total_relative':
                if not hasattr(self, 'curt_total_abs'):
                    curt_total_abs = self.getRESCurtailment('total_absolute')
                else:
                    curt_total_abs = self.curt_total_abs.copy()
                if not hasattr(self, 'res_gen_total'):
                    res_gen_total = self.getRESGeneration('total')
                else:
                    res_gen_total = self.res_gen_total.copy()

                curt_total_rel = {}
                for tech, curt_df in curt_total_abs.items():
                    curt_total_rel[tech] = curt_df.div(res_gen_total[tech], fill_value=float(0)) * 100
                    
                self.curt_total_rel = curt_total_rel
                return self.curt_total_rel
        except:
            raise
            print('E:getRESCurtailment')
            
    def getDualMinResConstraint(self, constraint='minRES'):
        try:
            df = self.model_results.copy()
            minRES = df[df['Symbol'] == constraint]
            minRES.drop(df.columns.difference(['Marginal', 'scenario']), axis=1, inplace=True)
            minRES.set_index('scenario', inplace=True)
            minRES = self.resultList2Matrix(minRES, col_idx=0)
            return minRES
        except:
            raise
            print('E:getDualMinResConstraint')
        
    def getGenerationProfiles(self):
        try:
            if not hasattr(self, 'hourly_demand_scen'):
                hourly_demand = self.getDemand(kind='hourly_scen')
            else:
                hourly_demand = self.hourly_demand_scen.copy()
            if not hasattr(self, 'res_avail_gen_hourly'):
                res_avail_gen_hourly = self.getRESAvailableGeneration('hourly')
            else:
                res_avail_gen_hourly = self.res_avail_gen_hourly.copy()
            if not hasattr(self, 'res_gen_hourly'):
                res_gen_hourly = self.getRESGeneration('hourly')
            else:
                res_gen_hourly = self.res_gen_hourly.copy()
            if not hasattr(self, 'sto_discharge_hourly'):
                sto_discharge_hourly = self.getStorageDischarge('hourly')
            else:
                sto_discharge_hourly = self.sto_discharge_hourly.copy()
            if not hasattr(self, 'sto_charge_hourly'):
                sto_charge_hourly = self.getStorageCharge('hourly')
            else:
                sto_charge_hourly = self.sto_charge_hourly.copy()
            if not hasattr(self, 'con_gen_hourly'):
                con_gen_hourly = self.getConGeneration('hourly')
            else:
                con_gen_hourly = self.con_gen_hourly.copy()
            if not hasattr(self, 'el_prices'):
                el_prices = self.getShadowPrices(constraint='energy_balance')
            else:
                el_prices = self.el_prices.copy()
            if not hasattr(self, 'curt_hourly'):
                curt_hourly = self.getRESCurtailment('hourly')
            else:
                curt_hourly = self.curt_hourly.copy()
            if not hasattr(self, 'sto_level_hourly'):
                sto_level_hourly = self.getStorageLevel()
            else:
                sto_level_hourly = self.sto_level_hourly.copy()
                        
            # merge time series
            generation_profiles = {}
            for scen, dem_df in hourly_demand.items():                
                res_avail_df = [data_dc[scen] for tech, data_dc in res_avail_gen_hourly.items()]
                res_df = [data_dc[scen] for tech, data_dc in res_gen_hourly.items()]
                res_cu_df = [data_dc[scen] for tech, data_dc in curt_hourly.items()]
                con_df = [data_dc[scen] for tech, data_dc in con_gen_hourly.items()]
                sto_df = [sto_discharge_hourly[scen], sto_charge_hourly[scen], sto_level_hourly[scen]]
                df_list = [dem_df] + res_df + res_avail_df + res_cu_df + con_df + sto_df
                df = pd.concat(df_list, axis=1)
                df.fillna(0, inplace=True)
                generation_profiles[scen] = df
            
            # merge prices with generation profiles
            profiles = {}
            el_prices = {scen: df for scen, df in el_prices.groupby('scenario')} 
            for scen, df in generation_profiles.items():
                df = df.merge(el_prices[scen]['Electricity Price [EUR/MWh]'], left_index=True, right_index=True)
                profiles[scen] = df
                            
            self.profiles = profiles
            return self.profiles
        except:
            raise
            print('E:getGenerationProfiles')

    def getResidualLoad(self, kind='before_curt_sto'):
        '''
        TODO: dynamic merge! p2x

        Parameters
        ----------
        kind : TYPE, optional
            DESCRIPTION. The default is 'before_curt_sto'.

        Returns
        -------
        residual_load_dc : TYPE
            DESCRIPTION.

        ''' 
        try:
            if not hasattr(self, 'generation_profiles'):
                generation_profiles = self.getGenerationProfiles()
            else:
                generation_profiles = self.generation_profiles.copy()
            
            # TODO: make dynamic
            res_avail_names = [str(tech + '_avail.l [MW]') for tech in self.res_avail_gen_hourly.keys()]
            res_gen_names = [str('G_' + tech + '.l [MW]') for tech in self.res_gen_hourly.keys()]
                         
            if kind == 'before_curt_sto':
                # compute residual load duration curve
                rldc = {}
                for scen, df in generation_profiles.items():
                    df['Residual_Load [MW]'] = df['d [MW]'] - df[res_avail_names].sum(axis=1)
                    rldc[scen] = df['Residual_Load [MW]'].sort_values(axis=0, ascending=False) 
                self.rldc_before_curt_sto = rldc
                return self.rldc_before_curt_sto
            
            elif kind == 'after_curt_before_sto':
                # compute residual load duration curve
                rldc = {}
                for scen, df in generation_profiles.items():
                    df['Residual_Load [MW]'] = df['d [MW]'] - df[res_gen_names].sum(axis=1)
                    rldc[scen] = df['Residual_Load [MW]'].sort_values(axis=0, ascending=False) 
                self.rldr_after_curt_before_sto = rldc
                return self.rldr_after_curt_before_sto
                                
            elif kind == 'after_curt_sto':
                # compute residual load duration curve
                rldc = {}
                for scen, df in generation_profiles.items():
                    df['Residual_Load [MW]'] = df['d [MW]'] - df[res_gen_names].sum(axis=1) - (df['STO_OUT.l [MW]'] - df['STO_IN.l [MW]'])
                    rldc[scen] = df['Residual_Load [MW]'].sort_values(axis=0, ascending=False) 
                self.rldr_after_curt_sto = rldc
                return self.rldr_after_curt_sto
            
            elif kind == 'after_p2x':
                pass
            elif kind == 'after_curt_sto_p2x':
                pass
        except:
            raise
            print('E:getResidualLoad')

    def getConFLH(self):
        '''
        TODO: maybe merge with RES and STO
        '''
        try:
            if not hasattr(self, 'con_gen_total'):
                con_gen_total = self.getConGeneration('total')
            else:
                con_gen_total = self.con_gen_total.copy()
            if not hasattr(self, 'con_capacity'):
                con_capacity = self.getConCapacity()
            else:
                con_capacity = self.con_capacity.copy()   
            
            con_flh = {}
            for tech, df in con_gen_total.items():
                _df = (df * 1e3) / con_capacity[tech]
                con_flh[tech] = _df.fillna(0).round()
        
            self.con_flh = con_flh
            return self.con_flh
        except:
            print('E:getConFLH')
    

    def getStorageFLH(self):
        try:
            if not hasattr(self, 'sto_total_discharge'):
                sto_total_discharge = self.getStorageDischarge('total')
            else:
                sto_total_discharge = self.sto_total_discharge.copy()
            if not hasattr(self, 'sto_cap_p'):
                sto_cap_p_out = self.getStorageCapacityPower('OUT')
            else:
                sto_cap_p_out = self.sto_cap_p_out.copy()   
            df = (sto_total_discharge * 1e3) / sto_cap_p_out
            df.fillna(0, inplace=True)
            df = df.round()
            self.storage_flh = df
            return self.storage_flh
        except:
            print('E:getStorageFLH')
            
    def getRESFLH(self):
        try:
            if not hasattr(self, 'res_gen_total'):
                res_gen_total = self.getRESGeneration('total')
            else:
                res_gen_total = self.res_gen_total.copy()
            if not hasattr(self, 'res_capacity'):
                res_capacity = self.getRESCapacity('compact')
            else:
                res_capacity = self.res_cap_compact.copy()   
            
            res_flh = {}
            for tech, df in res_gen_total.items():
                _df = (df * 1e3) / res_capacity[tech]
                res_flh[tech] = _df.fillna(0).round()
            
            self.res_flh = res_flh
            return self.res_flh
        except:
            print('E:getRESFLH')
            
    def getMarketValue(self):
        # TODo: for single tech
        try:
            profiles = self.getGenerationProfiles()
            dc = {}
            for scen, df in profiles.items():
                techs = [col for col in df if col.startswith('G_')]
                dc[scen] = {}
                for tech in techs:
                    pattern = 'G_(.*?)\.l'
                    name = re.search(pattern, tech).group(1)
                    name = name + ' [EUR/MWh]'
                    denominator = df[tech].sum()
                    if denominator != 0:
                        numerator = sum(df['Electricity Price [EUR/MWh]'] * df[tech])
                        dc[scen][name] = numerator / denominator
                    elif denominator == 0:
                        dc[scen][name] = 0
                denominator = df['STO_OUT.l [MW]'].sum()
                if denominator != 0:
                    numerator = sum(df['STO_OUT.l [MW]'] * df['Electricity Price [EUR/MWh]'])
                    dc[scen]['storage [EUR/MWh]'] = numerator / denominator
                elif denominator == 0:
                    dc[scen]['storage [EUR/MWh]'] = 0
                dc[scen] = pd.DataFrame.from_dict(dc[scen], orient='index').squeeze().rename(scen)
            
            mv = pd.DataFrame()
            
            for scen, s in dc.items():
                mv = pd.concat([mv, s], axis=1)
            
            self.mv = mv.T
            return self.mv
        except:
            raise
            print('E:getMarketValue')



# =============================================================================
#     def getRESStored(self):
#         try:
#             pass
#         except:
#             raise
#             print('E:getRESStored')
#
#     
#     def getRESStorageLoss(self):
#         try:
#             pass
#         except:
#             raise
#             print('E:getRESStorageLoss')
#
#     def getRESTotalLoss(self):
#         try:
#             pass
#         except:
#             raise
#             print('E:getRESStorageLoss')
# =============================================================================


    def heatMap(self, data, x_label, y_label, cbar_label, figsize, font_scale, cmap='PuBu'):
        try:
            # convert data to np.float64
            data = pd.DataFrame(data.to_numpy(dtype=np.float64),
                                index=data.index, columns=data.columns)
            # plot heat map
            sns.set(font_scale=font_scale, font='serif')
            fig = plt.figure(figsize=figsize)
            heatmap = sns.heatmap(data, cmap=cmap, cbar_kws={'label': cbar_label})
            heatmap.set_yticklabels(heatmap.get_yticklabels(), rotation=0)
            heatmap.tick_params(labelsize=10)
            cax = plt.gcf().axes[-1]
            cax.tick_params(labelsize=10)
            plt.subplots_adjust(top=0.98, bottom=0.174, left=0.057, right=1.0, hspace=0.0, wspace=0.1)
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            plt.tight_layout()
            plt.show()
        except:
            raise
            print('E:heatMap')

    def plotHeatMap(self, df, x_label, y_label, cbar_label, cmap='PuBu', figsize=(12,5), font_scale=1.2, scenarios='all', selection=None, exclusion=None):
        '''
        selection : list, optional
            Selection of iterated constraints. The default is None.
        scenarios : TYPE, optional
            'all' -> plot all constraints
            'no_storage_cycling' -> plot only those w/o storage cycling
            'selected' = plot only selection
            'excluding' = plot all but the excluded constraints in selection
            The default is 'all'.

        Parameters
        ----------
        df : pandas dataframe
            Holds data, rows relate to iterated parameter, columns to iterated
            constraints.
        x_label : str
            Label for x_axes.
        y_label : str
            Label for y_axes.
        cbar_label : str
            Label for cbar_axes.
        scenarios : str, optional
            Which constraints should be plotted. The default is 'all'.
            'all' -> plot all constraints
            'no_storage_cycling' -> plot only those w/o storage cycling
            'selected' = plot only selection, specify selection
            'excluding' = plot all but the excluded constraints, specify exclusion
        selection : list of constraint names, optional
            List of constraint names (dtype=str). The default is None.
        exclusion : list of constraint names, optional
            List of constraint names (dtype=str). The default is None.

        Returns
        -------
        Plot of heatmap.

        '''
        try:
            if scenarios == 'all':
                self.heatMap(df, x_label, y_label, cbar_label, figsize, font_scale, cmap)
            elif scenarios == 'no_storage_cycling':
                if not hasattr(self, 'cons_no_sto_cyc'):
                    no_sto_cyc = self.getStorageCycling('number')
                else:
                    no_sto_cyc = self.cons_no_sto_cyc.copy()
                df = df[no_sto_cyc].copy()
                self.heatMap(df, x_label, y_label, cbar_label, figsize, font_scale, cmap)
            elif scenarios == 'selected':
                 df = df[selection].copy()
                 self.heatMap(df, x_label, y_label, cbar_label, figsize, font_scale, cmap)
            elif scenarios == 'excluding':
                df = df.drop(exclusion, axis=1).copy()
                self.heatMap(df, x_label, y_label, cbar_label, figsize, font_scale, cmap)
        except:
            raise
            print('E:plotHeatMap')

    def plotSingleBars(self, data, x_label, y_label, bicolor=True):
        try:
            rcParams['font.family'] = 'serif'
            y_pos = np.arange(len(data))
            fig = plt.figure(figsize=(4,3))

            if bicolor:
                if not hasattr(self, 'cons_no_sto_cyc'):
                    no_sto_cyc = self.getStorageCycling('number')
                mask = [idx in self.cons_no_sto_cyc for idx in data.index]
                not_mask = [not i for i in mask]
                ax = plt.subplot(111)
                ax.bar(y_pos[mask], data[mask], label='no storage cycling', color='teal', align='center', alpha=0.7)
                ax.bar(y_pos[not_mask], data[not_mask], label='storage cycling', color='cadetblue', align='center', alpha=0.7)
                box = ax.get_position()
                ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
                ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
                          frameon=False, ncol=2)
            else:
                plt.bar(y_pos, data, align='center', alpha=0.7, color='cadetblue')
            plt.subplots_adjust(top=0.95, bottom=0.264, left=0.135, 
                                right=0.912, hspace=0.2, wspace=0.2)
            plt.xticks(y_pos, data.index)
            plt.ylabel(y_label, fontsize=11)
            plt.xlabel(x_label, fontsize=11)
            plt.tight_layout()
            plt.show()
        except:
            raise
            print('E:plotSingleBars')
            
    def plotHourlyProfiles(self, labels, colors, kind='generation_only', sto_level=False, df=None, df_area=None, df_line=None, legend=True, legend_position='upper center', legend_col_no=8, figsize=(14,3), bottom_fix=0.3, fontsize=14, ticksize=12, legfontsize=12):
        try:
            rcParams['font.family'] = 'serif'
            
            if kind == 'generation_only':
                ax = df.plot.area(color=colors, figsize=figsize, stacked=True)
                # set line width to zero
                for line in ax.lines:
                    line.set_linewidth(0)
                # Shrink current axis's height by 20% on the bottom
                box = ax.get_position()
                ax.set_position([box.x0, box.y0 + box.height * 0.2,
                                 box.width, box.height * 0.8])
                # Put a legend below current axis
                leg = ax.legend(labels=labels, loc=legend_position, bbox_to_anchor=(0.5, -0.2), ncol=legend_col_no, frameon=False)
                for line in leg.get_lines():
                    line.set_linewidth(8)
                plt.ylabel('GW', fontsize=fontsize)
                plt.xlabel('hour', fontsize=fontsize)
                
            elif kind == 'generation_price_rl':
                fig, ax1 = plt.subplots(figsize=figsize)
                ax2 = ax1.twinx()
                
                # stacked area plot
                x = range(int(df_area.index[0]), int(df_area.index[-1])+1)
                df_area_neg, df_area_pos = df_area.clip(upper=0), df_area.clip(lower=0)
                y_pos = [df_area_pos[c].tolist() for c in df_area_pos]
                y_neg = [df_area_neg[c].tolist() for c in df_area_neg]
                area1 = ax1.stackplot(x, y_pos, labels=labels, colors=colors)
                area2 = ax1.stackplot(x, y_neg, colors=colors)
                
                # line plot1
                line1 = ax1.plot(df_line['residual load [MW]'], color='k', linestyle='--', label='residual load')
                
                if sto_level:
                    # line plot1
                    line2 = ax1.plot(df_line['STO_L.l [MWh]'], color='#434566', linestyle='-.', label='storage level')
                
                # line plot2
                line3 = ax2.plot(df_line['Electricity Price [EUR/MWh]'], color='r', label='electricity price')
                
                # legend
                if legend:
                    figs = area1 + area2 + line1 + line3
                    if sto_level:
                        figs = area1 + area2 + line1 + line2 + line3
                    labels_all = [f.get_label() for f in figs]
                    leg = ax1.legend(figs, labels_all, loc=legend_position, bbox_to_anchor=(0.5, -0.25), ncol=legend_col_no, fontsize=legfontsize, frameon=False)
                
                # properties
                ax1.set_xlabel('hour', fontsize=fontsize) 
                ax1.set_ylabel('GW', fontsize=fontsize)
                if sto_level:
                    ax1.set_ylabel('GW/GWh', fontsize=fontsize)
                ax2.set_ylabel('price [EUR/MWh]', fontsize=fontsize)
                ax1.tick_params(axis='both', which='major', labelsize=ticksize)
                ax2.tick_params(axis='y', which='major', labelsize=ticksize)

            plt.margins(x=0)
            plt.tight_layout()
            plt.subplots_adjust(bottom=bottom_fix, top=0.99, left=0.05, right=0.95)
            plt.show()
            return fig, ax1
        except:
            raise
            print('E:plotHourlyProfiles')
            
    def plotSankey(self, constraint, parameter, node_distance=20, node_thickness=10, kind='aggregated_loss'):
        try:
            # retrieve data
            res_gen = self.getRESGeneration('total')
            pv_gen = res_gen['pv']
            wind_gen = res_gen['wind']
            res_avail_gen = self.getRESAvailableGeneration('total')
            pv_avail_gen = res_avail_gen['pv']
            wind_avail_gen = res_avail_gen['wind']
            wind_cu = wind_avail_gen - wind_gen
            pv_cu = pv_avail_gen - pv_gen
            cu_abs = self.getRESCurtailment('total_absolute')
            cu_test_wind = cu_abs['wind'] - wind_cu
            cu_test_pv = cu_abs['pv'] - pv_cu
            con_gen_total = self.getConGeneration('total')
            coal = con_gen_total['coal']
            ocgt = con_gen_total['ocgt']
            dem = self.getDemand('total')
            sto_gen = self.getStorageGeneration('total')
            sto_gen_in = sto_gen['STO_IN_total.l [TWh]']
            sto_gen_out = sto_gen['STO_OUT_total.l [TWh]']
            spc_total = self.getStorageCycling('spc_total')
            ppc_total = self.getStorageCycling('ppc_total')
            spc_cyc_e = self.getStorageCycling('spc_cyc_e')
            ppc_cyc_e = self.getStorageCycling('ppc_cyc_e')
            spc_loss_total = self.getStorageCycling('spc_loss_total')
            ppc_loss_total = self.getStorageCycling('ppc_loss_total')
            sto_cyc_total = spc_total + ppc_total
            USL = self.getStorageCycling('loss_total')
            sto_loss = self.getStorageLoss()
            int_sto_loss = sto_loss - spc_loss_total - ppc_loss_total
            int_sto_gen_in = sto_gen_in - sto_cyc_total
            int_sto_gen_out = sto_gen_out - spc_cyc_e - ppc_cyc_e
            
            if kind == 'aggregated_loss':
                # construction flows       
                source = [0, 0, 1, 1,       # RES generation potential: wind_avail, pv_avail
                          2, 3,             # conventional generation: coal, ocgt
                          5, 6,             # RES generation: wind, pv
                          7,                # Electricity in grid
                          7, 7,             # Electricity in grid
                          9, 9,             # storage discharge
                          9, 9]             # storage discharge
                 
                target = [4, 5, 4, 6,       # RES conversion: cu_wind, wind_gen, cu_pv, pv_gen, 
                          7, 7,             # grid from conventionals
                          7, 7,             # grid from RES
                          8,                # final energy consumption
                          9, 9,             # storage charge
                          10, 11,           # storage loss (plausible, unintended)
                          7, 7]             # storage discharge
                
                value = [wind_cu.loc[parameter, constraint], wind_gen.loc[parameter, constraint], pv_cu.loc[parameter, constraint], pv_gen.loc[parameter, constraint],
                         coal.loc[parameter, constraint], ocgt.loc[parameter, constraint],
                         wind_gen.loc[parameter, constraint], pv_gen.loc[parameter, constraint],
                         dem.loc[parameter, constraint], 
                         spc_total.loc[parameter, constraint] + ppc_total.loc[parameter, constraint], int_sto_gen_in.loc[parameter, constraint], 
                         int_sto_loss.loc[parameter, constraint], spc_loss_total.loc[parameter, constraint] + ppc_loss_total.loc[parameter, constraint],
                         spc_cyc_e.loc[parameter, constraint] + ppc_cyc_e.loc[parameter, constraint], int_sto_gen_out.loc[parameter, constraint]]
                
                color_link = ['#D3D3D3', '#8ca3ab', '#D3D3D3', '#ffffbb',
                              '#bb8874', '#ff814b',
                              '#8ca3ab', '#ffffbb',
                              '#c2f08e',
                              '#ff8585', '#c2f08e',
                              '#D3D3D3', '#ff8585',
                              '#ff8585', '#c2f08e']
                
                # construction nodes
                label = ['wind generation potential', 
                         'pv generation potential', 
                         'coal generation', 
                         'ocgt generation', 
                         'curtailment', 
                         'wind generation',
                         'pv generation',
                         'electricity grid', 
                         'final energy',
                         'storage', 
                         'plausible storage losses',
                         'unintended storage losses']
                
                color_node = ['#518696', 
                              '#fffb4e', 
                              '#895a47',
                              '#f95827',
                              '#8e8eb5',
                              '#518696',
                              '#fffb4e',
                              '#4b830d',
                              '#316a00',
                              '#434566',
                              '#434566',
                              '#434566']
                
            elif kind == 'detailed_loss':
                # construction flows      
                source = [0, 0, 1, 1,       # RES generation potential: wind_avail, pv_avail
                          2, 3,             # conventional generation: coal, ocgt
                          5, 6,             # RES generation: wind, pv
                          7,                # Electricity in grid
                          7, 7,             # Electricity in grid
                          9, 9, 9,          # storage discharge
                          9, 9]             # storage discharge
                
                target = [4, 5, 4, 6,       # RES conversion: cu_wind, wind_gen, cu_pv, pv_gen, 
                          7, 7,             # grid from conventionals
                          7, 7,             # grid from RES
                          8,                # final energy consumption
                          9, 9,             # storage charge
                          10, 11, 12,       # storage loss (plausible, unintended SPC, unintended APC)
                          7, 7]             # storage discharge
                
                value = [wind_cu.loc[parameter, constraint], wind_gen.loc[parameter, constraint], pv_cu.loc[parameter, constraint], pv_gen.loc[parameter, constraint],
                         coal.loc[parameter, constraint], ocgt.loc[parameter, constraint],
                         wind_gen.loc[parameter, constraint], pv_gen.loc[parameter, constraint],
                         dem.loc[parameter, constraint], 
                         spc_total.loc[parameter, constraint] + ppc_total.loc[parameter, constraint], int_sto_gen_in.loc[parameter, constraint], 
                         int_sto_loss.loc[parameter, constraint], spc_loss_total.loc[parameter, constraint], ppc_loss_total.loc[parameter, constraint],
                         spc_cyc_e.loc[parameter, constraint] + ppc_cyc_e.loc[parameter, constraint], int_sto_gen_out.loc[parameter, constraint]]
    
                color_link = ['#D3D3D3', '#8ca3ab', '#D3D3D3', '#ffffbb',
                              '#bb8874', '#ff814b',
                              '#8ca3ab', '#ffffbb',
                              '#c2f08e',
                              '#ff8585', '#c2f08e',
                              '#D3D3D3', '#ff8585', '#ff8585',
                              '#ff8585', '#c2f08e']
                                
                # construction nodes
                color_node = ['#518696', 
                              '#fffb4e', 
                              '#895a47',
                              '#f95827',
                              '#8e8eb5',
                              '#518696',
                              '#fffb4e',
                              '#4b830d',
                              '#316a00',
                              '#434566',
                              '#434566',
                              '#434566']
                
                label = ['wind generation potential', 
                         'pv generation potential', 
                         'coal generation', 
                         'ocgt generation', 
                         'curtailment', 
                         'wind generation',
                         'pv generation',
                         'electricity grid', 
                         'final energy',
                         'storage', 
                         'plausible storage losses',
                         'unintended storage losses (SPC)',
                         'unintended storage losses (APC)']
                
            # link is flow data
            link = dict(source = source, target = target, value = value, color=color_link) #, customdata = customdata_link, hovertemplate = hovertemplate_link)
            
            # node is node data
            node = dict(label = label, pad=node_distance, thickness=node_thickness, color=color_node)
        
            # construct sankey
            data = go.Sankey(valueformat=".0f", valuesuffix="TWh", link=link, node=node)
            fig = go.Figure(data)
            fig.update_layout(
                hovermode = 'x',
                font_family="Times New Roman",
                font=dict(size = 25)#, color = 'white'),
            )
            #fig.update_traces(mode="name")
            fig.show()
            fig_name = 'sankey_{}_{}.html'.format(parameter, constraint)
            fig.write_html(fig_name)
            #plot(fig)
        except:
            raise
            print('E:plotSankey')
            
    def plotViolinDistribution(self, data, x_category, y_category, condition_on, bandwidth, scale='count', ncol=5, figsize=(18, 10), bbox_to_anchor=(0.5, -0.15), split=True, palette='mako'):
        try:
            # figure settings
            plt.rcdefaults()
            rcParams['font.family'] = 'serif'
            plt.rc('font', size=12)          # controls default text sizes
            plt.rc('axes', titlesize=14)     # fontsize of the axes title
            plt.rc('axes', labelsize=14)     # fontsize of the x and y labels
            plt.rc('xtick', labelsize=12)    # fontsize of the tick labels
            plt.rc('ytick', labelsize=12)    # fontsize of the tick labels
            plt.rc('legend', fontsize=12)    # legend fontsize
            # plot figure
            f, ax = plt.subplots(figsize=figsize)
            # plot violine
            ax = sns.violinplot(x=x_category, 
                                y=y_category,
                                data=data,
                                hue=condition_on,
                                palette=palette,
                                split=split,
                                scale=scale,
                                #color='b',
                                #saturation=1,
                                bw=bandwidth,
                                linewidth=.6,
                                inner=None,
                                #cut=0
                                )
            # plot horizontal line
            ax.axhline(0, c='gray', linewidth=.6)#, ls='--')
            # plot bubbles
            sns.scatterplot(x=x_category,
                            y=y_category,
                            data=data,
                            #size=, # condition bubble size on category
                            palette=palette,
                            alpha=0.7,
                            #sizes=(1, 300), # bubble size spectrum
                            hue=condition_on
                            )
            
            # Shrink current axis's height by 20% on the bottom
            box = ax.get_position()
            ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
            #plt.subplots_adjust(top=0.995, bottom=0.18, left=0.059, right=0.998, hspace=0.2, wspace=0.2)
            plt.subplots_adjust(top=0.99,bottom=0.157,left=0.082,right=0.996,hspace=0.2,wspace=0.2)
            # Put a legend below current axis
            leg = ax.legend(loc=8, bbox_to_anchor=bbox_to_anchor, ncol=ncol, frameon=False)# fancybox=True)
            
            return f            
        except:
            raise
            print('E:plotViolinDistribution')


#%% main

if __name__ == '__main__':
    
    # here some examples for how to use the analysis tool
    path = "...\\report\\report" # config
    
    # create object and load results
    x = MGamsPyAnalysis(path)
    x.iter_parameters = {'phi_min_res': [0, 100, 10, 20, 30, 40, 50, 60, 70, 80, 90]}
    x.iter_constraints = ['1a', '1b', '1c', '2a', '2b', '2c', '3a', '3b', '3c', '4a', '4b', '4c']
    
    # replace lengthy scenario names
    x.createScenarioList()
    x.getScenarioList()
    x.replaceScenarioNames()
    x.getScenarioList()
    
    # plot parameters
    x_label = 'model specification'
    yx_label = 'RES share [%]'
    yy_label = 'efficiency'
    plt.rcdefaults()
    rcParams['font.family'] = 'serif'
    
    # total system costs
    tc = x.getTotalSystemCosts()
    tc = tc.iloc[:, 0:3]
    tc.columns = ['zero', 'proportionate', 'complete']
    x.plotHeatMap(tc, x_label, yx_label, 'Total system cost [bn EUR]', scenarios='all')
    x.plotSingleBars(tc.loc[80, :], 'SLCR', 'total system cost [bn EUR]', bicolor=False)
    
    # prices
    neg_prices = x.getNegativePrices(x.getShadowPrices(constraint='energy_balance'))
    dual_minRES = x.getDualMinResConstraint(constraint='minRES')
    x.plotSingleBars(neg_prices.loc[80, :], x_label, 'occurances')
    x.plotSingleBars(dual_minRES.loc[80, :], x_label, 'shadow price [EUR/MWh]')