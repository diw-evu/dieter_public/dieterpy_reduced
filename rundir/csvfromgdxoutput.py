import os
import gdxpds
import pandas as pd
import gc


def to_csv(outgdxpath):
    f = os.path.split(outgdxpath)[-1]
    destdir = os.path.join('..', 'csv', f[:-4])
    os.makedirs(destdir, exist_ok=True)
    dataframes = gdxpds.to_dataframes(outgdxpath)
    with gdxpds.gdx.GdxFile(lazy_load=False) as j:
        j.read(outgdxpath)
        for sym in j:
            k = sym.__dict__['_name']
            dims = sym.__dict__['_dims']
            v = dataframes[k]
            sterisk = '*'
            if sterisk not in dims:
                v.to_csv(os.path.join(destdir, '.'.join([*dims, k, 'csv'])), index=False)
            else:
                v.to_csv(os.path.join(destdir, '.'.join([k, 'csv'])), index=False)
    del dataframes


def makedictsets(f, omittedsets=None):
    setord = {}
    for symbol in f:
        GamsDataType = symbol.data_type.name
        if GamsDataType == 'Set':
            if omittedsets is not None:
                flag = False
                for st in omittedsets:
                    # be careful, the string st can be in any part of the set name
                    if st in symbol.name:
                        flag = True
                        continue
                if flag:
                    continue
            df = symbol.dataframe
            # assumption: only 1D sets
            symbol_set = frozenset(df.iloc[:, 0].to_list())
            setord[symbol.name] = list(symbol_set)
    return setord


def makedictsymbols(f, setord, omittedsymb=None):
    frames = {}
    heads = ['Value', 'Level', 'Marginal', 'Lower', 'Upper', 'Scale']
    for symbol in f:
        GamsDataType = symbol.data_type.name
        if GamsDataType != 'Set':
            dims = symbol.dims
            df = symbol.dataframe.copy()
            if df.empty:
                continue
            else:
                if omittedsymb is not None:
                    flag = False
                    for omit in omittedsymb:
                        # be careful, the string omit can be in any part of the symbol name
                        if omit in symbol.name:
                            flag = True
                            continue
                    if flag:
                        continue
                if '*' in dims:
                    cols = []
                    for j, c in enumerate(df.columns):
                        if '*' == c:
                            cols.append('*' + str(j))
                            continue
                        cols.append(c)
                    df.columns = cols
                    ixtodrop = []
                    ordered = []
                    for i, cname in enumerate(cols):
                        if cname not in heads:
                            if cname.startswith('*'):
                                ixtodrop.append(cname)
                                colnow = set(df.iloc[:, i].to_list())
                                for k, v in setord.items():
                                    if colnow.issubset(set(v)):
                                        df.loc[:, k] = df.iloc[:, i].values
                                        ordered.append(k)
                            else:
                                ordered.append(cname)
                        else:
                            ordered.append(cname)
                    df.loc[:, GamsDataType] = True
                    df.drop(ixtodrop, axis=1, inplace=True)
                    df = df[ordered].copy()
                if 'Value' in df.columns.to_list():
                    df.rename(columns={'Value': 'Level'}, inplace=True)
            df.loc[:, GamsDataType] = 1
            frames[symbol.name] = df.round(3)
    return frames


def createbigdf(frames, setord, droplevelzero=True):
    output = pd.DataFrame()
    for symbolname, df in frames.items():
        df.loc[:, 'Symbol'] = symbolname
        output = output.append(df, sort=False)
    ordls = []
    for k in setord.keys():
        if k in output.columns.to_list():
            ordls.append(k)
    for k in ['Set', 'Parameter', 'Alias', 'Variable', 'Equation', 'Symbol', 'Level', 'Marginal', 'Lower', 'Upper', 'Scale']:
        if k in output.columns.to_list():
            ordls.append(k)
    for k in output.columns.to_list():
        if k not in ordls:
            print("This symbol '{}' has been left behind ".format(k))
    output.reset_index(drop=True, inplace=True)
    if 'h' in output.columns.to_list():
        output.loc[:, 'h'] = output['h'].str.slice(1).astype(float).astype('Int32')
    if droplevelzero:
        output.drop(output[(output['Level'] == 0.0) & (output['Marginal'] == 0.0)].index, axis=0, inplace=True)
    return output[ordls].dropna(axis=1, how='all').reset_index(drop=True).copy()


def to_bigcsv(outgdxpath, destdir=None, omittedsets=None, omittedsymb=None):
    scen = os.path.split(outgdxpath)[-1][:-4]
    if destdir is None:
        destdir = ''
    os.makedirs(destdir, exist_ok=True)
    fp = os.path.join(destdir, scen)
    with gdxpds.gdx.GdxFile(lazy_load=False) as f:
        f.read(outgdxpath)
        setdc = makedictsets(f, omittedsets=omittedsets)
        symdc = makedictsymbols(f, setdc, omittedsymb=omittedsymb)
        final = createbigdf(symdc, setdc, droplevelzero=False)
        final.to_csv(fp + '.csv.gz', index=False, compression='gzip')
    del setdc, symdc, final
    gc.collect()
    return None


def reports(query, rep_read, rep_dest=None):
    '''
    Example of query:
    'query = "Variable == 1 | Symbol == 'd' | Symbol == 'con1a_bal' | Symbol == 'con9a_reserve_prov_endogenous' | Symbol == 'con9b_reserve_prov_PR_endogenous' | Symbol == 'con5c_max_CO2'"'
    '''
    if rep_dest is None:
        rep_dest = ''
    os.makedirs(rep_dest, exist_ok=True)
    fpathlist = [os.path.join(rep_read, f) for f in os.listdir(rep_read) if os.path.isfile(os.path.join(rep_read, f))]
    fnames = [os.path.split(file)[-1].split('+')[0] for file in fpathlist]
    report = pd.DataFrame()
    for fp, fname in zip(fpathlist, fnames):
        df = pd.read_csv(fp, dtype={'year': 'Int32', 'h': 'Int32', 'Set': 'Int32', 'Parameter': 'Int32', 'Alias': 'Int32', 'Variable': 'Int32', 'Equation': 'Int32'})
        mask = df.eval(query)
        mask.fillna(False, inplace=True)
        df = df[mask].copy()
        df.loc[:, 'scenario'] = fname
        report = report.append(df, sort=False)
    report.reset_index(drop=True, inplace=True)
    report.drop(['Lower', 'Upper', 'Scale'], axis=1, inplace=True)
    report.drop(report[(report['Level'] == 0.0) & (report['Marginal'] == 0.0)].index, axis=0, inplace=True)
    report.dropna(axis=1, how='all', inplace=True)
    report.to_csv(os.path.join(rep_dest, 'report.csv'), index=False)
    del df, report
    gc.collect()