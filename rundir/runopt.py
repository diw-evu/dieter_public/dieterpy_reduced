from gams import GamsWorkspace, GamsOptions, DebugLevel
import itertools
import os
import sys
import time
from datetime import datetime
from exceltogdx import exceltogdx
import pandas as pd
from csvfromgdxoutput import to_csv, to_bigcsv, reports  # This cause in windows sometimes fails.
# see https://conda.io/projects/conda/en/latest/user-guide/troubleshooting.html#numpy-mkl-library-load-failed


def is_non_zero_file(fpath): 
    '''
    Checks whether a file exits under given path and whether its size is larger 
    than two bites. Csv-files larger than two bites are non-empty.

    Parameters
    ----------
    fpath : string
        Path to the file.

    Returns
    -------
    TYPE: Boolean
        Boolean indicating whether a file exits and whether its larger than 
        two bites.

    '''
    try:    
        return os.path.isfile(fpath) and os.path.getsize(fpath) > 2
    except:
        print('E:is_non_zero_file\n')
        raise

def getConfigVariables(input_path, sheet_name):
    '''
    Imports configuration variables specifying model control variables, 
    scenario variables, iteration constraints and iteration parameters.

    Parameters
    ----------
    path : string
        Path referring to folder location that contains the config file, e.g. 
        '../input'.
    sheet_name : string
        Sheet name in config file.

    Returns
    -------
    cv_dict : 
        Dictionary containing configuration variables.
    '''
    try:    
        cv_df = pd.read_excel(str(input_path+'/config.xlsx'), sheet_name=sheet_name)
        cv_dict = {}
        for i, row in cv_df.iterrows():
            cv_dict[row['feature']] = row['value'] if not pd.isnull(row['value']) else ''
        
        return cv_dict    
    except:
        print('E:getConfigVariables\n')
        raise
        
def generateInputGDX(input_path, gdx_path, convar_dc):
    '''
    Generates input gdx files if skip_import == 'no'.

    Parameters
    ----------
    input_path : string
        Path referring to folder location that contains input xlsx files.
    gdx_path : string
        Path referring to folder location that contains input gdx files to be 
        generated.
    convar_dc : dict
        contains info on skip_import (either "yes" or "no").

    Raises
    ------
    Exception
        If neither "yes" nor "no" is entered as value for control variable 
        "skip_import" in the config file.

    Returns
    -------
    None.

    '''
    try:        
        if convar_dc['skip_input'] == 'no':
            if convar_dc['data_input_file']:
                print('Import from ' + convar_dc['data_input_file'] + ' commences.')
                exceltogdx(os.path.join(input_path, convar_dc['data_input_file']), str(gdx_path+'/data_input.gdx'))
            else:
                print('No upload time-invariant data chosen.')
            if convar_dc['time_series_file']:
                print('Import from ' + convar_dc['time_series_file'] + ' commences.')
                exceltogdx(os.path.join(input_path, convar_dc['time_series_file']), str(gdx_path+'/time_series.gdx'))
            else:
                print('No upload of time-varinat data chosen.')
        elif convar_dc['skip_input'] == 'yes':
            pass
            # how to tell GAMS which gdx files shall be used?
        else:
            raise Exception('Choose skip_input "yes" or "no" in config file on sheet control_variables')
    except:
        print('E:generateInputGDX\n')
        raise
        
def getIteratableParameters(convar_dc, input_path, sheet_name):
    '''
    Generates dictionaries containing parameter and according parameter
    values to be looped over in GAMS scenario runs.

    Parameters
    ----------
    convar_dc : dict
        Contains information on control variable "iteration_parameter".
    input_path : string
        Path referring to folder location that contains the congif file.
    sheet_name : string
        Sheet in config file listing all iterables.

    Raises
    ------
    Error indicator to the function.

    Returns
    -------
    iterdict : dictionary
        Contains iterables information.
    '''
    
    try:
        iterdict = {}
        if convar_dc[sheet_name] == 'yes':
            df_iter = pd.read_excel(str(input_path+'/config.xlsx'), sheet_name=sheet_name)
            for key in df_iter.columns:
                iterdict[key] = [col for col in df_iter[key].to_list() if not pd.isnull(col)]
        else:
            print('Iterable parameter control variable turned off.')
        return iterdict
    except:
        print('E:getIteratableParameters\n')
        raise   
        
def getIteratableConstraints(convar_dc, input_path, sheet_name):
    '''
    Generates dictionaries containing constraints to be looped over in GAMS 
    scenario runs.

    Parameters
    ----------
    convar_dc : dict
        Contains information on control variable "iteration_constraints".
    input_path : string
        Path referring to folder location that contains the congif file.
    sheet_name : string
        Sheet in config file listing all iterables.

    Raises
    ------
    Error indicator to the function.

    Returns
    -------
    iterdict : dictionary
        Contains iterables information. 'yes' indicates constraint iteration,
        'no' indicates that constraint is not to be used in scenario loops.
    '''
    
    try:
        #if convar_dc[sheet_name] == 'yes':
        df_iter = pd.read_excel(str(input_path+'/config.xlsx'), sheet_name=sheet_name)
        iterdict = {data[0]: data[1] for _, data in df_iter.iterrows()}
        #else:
        #    print('Iterable parameter control variable turned off.')
        return iterdict
    except:
        print('E:getIteratableConstraints\n')
        raise   
        
def paramScenarioMatrixQueue(iterparam_dc):
    '''
    Generates list of dictionary containing all parameter variations as matrix.
    Scenarios are the result of the vector product of all parameter values 
    specified in interparam_dc.

    Parameters
    ----------
    iterparam_dc : dict
        Contains parameter and values for scenario iterations. Multiple values 
        per parameter possible. 

    Returns
    -------
    queue : list of dicts
        Contains all combinations of all iteration parameter values multiplied
        as matrix.

    '''
    # list of dict containing full scenario matrix
    listofdict = []
    # prelist containing all variations for all parameters to be varied
    prelist = []
    for parameter, values in iterparam_dc.items():
        # templist containing all variations per single parameter
        templist = []
        for i in values:
            dct = {parameter: i}
            templist.append(dct)
        prelist.append(templist)
    # compile scenario matrix
    for comb in itertools.product(*prelist):
        dct = {}
        for i in range(len(prelist)):
            dct.update(comb[i])
        listofdict.append(dct)
    queue = listofdict
    return queue

def paramScenarioTupleQueue(iterparam_dc):
    '''
    Generates list of dictionary containing all parameter variations as matrix.
    Scenarios are the result of the vector product of all parameter values 
    specified in interparam_dc.

    Parameters
    ----------
    iterparam_dc : dict
        Contains parameter and values for scenario iterations. Multiple values 
        per parameter possible. 

    Returns
    -------
    queue : list of dicts
        Contains all scenario tuples of all iteration parameter values ('one per row)'.
    '''
    # list of all parameter values to be combined in one scenario
    combilist = []
    for combi in zip(*iterparam_dc.values()):
            combilist.append(combi)
    # list of all parameter names to be combined in one scenario
    paralist = list(iterparam_dc.keys())
    # combine parameter names with values in one tuple dictionary
    scenario_tuples = []
    for combi in combilist:
        tuple_dc = {}
        for idx, value in enumerate(combi):
            tuple_dc[paralist[idx]] = value
        scenario_tuples.append(tuple_dc)
    return scenario_tuples

def prepareGAMSAPI():
    '''
    Prepares GAMS API.

    Returns
    -------
    ws : GAMS workspace object
       Base class of the gams namespace, used for initiating GAMS objects
       (e.g. GamsDatabase and GamsJob) by an "add" method of GamsWorkspace.
       Unless a GAMS system directory is specified during construction of 
       GamsWorkspace, GamsWorkspace determines the location of the GAMS 
       installation automatically. Aorking directory (the anchor into the file
       system) can be provided when constructing the GamsWorkspace instance.
       It is used for all file-based operations.
    sysdir : string
        GAMS system directory path.
    cp : GAMS execution Checkpoint
        A GamsCheckpoint class captures the state of a GamsJob after the 
        GamsJob.run method has been carried out. Another GamsJob can continue 
        (or restart) from a GamsCheckpoint.
    opt : GAMS options object
        Stores and manages GAMS options for a GamsJob and GamsModelInstance.

    '''
    try:
        # define directories and create GAMS workspace
        working_directory = ".."
        ws = GamsWorkspace(working_directory=working_directory)
        sysdir = ws.get_system_directory()
        # get GAMS version 
        version = GamsWorkspace.api_version
        print('version: ', version)
        print('MainDir ---->: ', ws.working_directory)
        cp = ws.add_checkpoint()
        opt = GamsOptions(ws)
        return ws, sysdir, cp, opt
    except:
        raise
        print('E:prepareGAMSAPI')
    
        
def defineGAMSOptions(scen_dc, opt, itercon_dc, convar_dc, *args):
    '''
    Defines scenario and control options and hands them over to the GAMS 
    options object. If iteration_constraints is activated in config file, then
    the constraints are handed over, too.

    Parameters
    ----------
    scen_dc : dict
        Contains all scenario options.
    opt :GAMS options object
        Stores and manages GAMS options for a GamsJob and GamsModelInstance.
    itercon_dc : dict
        Contains all constraints to iterated.
    *args : string
        Optional constraint that is to be considered in scenario runs.

    Returns
    -------
    None.

    '''   
    try:
        # scenario options
        for k in scen_dc.keys():
            if (scen_dc[k] == 'yes') or (scen_dc[k] == 'no'):
                opt.defines[str('py_'+k)] = '*' if scen_dc[k] == 'yes' else '""'
                #opt.defines['py_feat'] = '"' + feat_str + '"'
                #print('^^^^ string Feat_node:', feat_str)
            else:
                opt.defines[str('py_'+k)] = "'{}'".format(str(scen_dc[k]))     
        
        # control_variable options
        opt.defines['py_threads'] = str(convar_dc['parallel_barrier_threads']) if convar_dc['thread'] == 'yes' else '1'
        opt.defines['py_nocross'] = '*' if convar_dc['no_crossover'] == 'yes' else '""'
        
        # handover iteration constraint options to GAMS        
        # test for empty iteration_constraint dict in config.xlsx
        if not itercon_dc:
            raise('Indicate on sheet "iteration_constraints" in the config.xlsx \
                  for all iteration_constraints (all py_constraints) whether they \
                  shall be considered as scenario (yes) or not (no).')
        # if there is contraint iteration
        if convar_dc['iteration_constraints'] == 'yes':
            itercon_dc_temp = itercon_dc.fromkeys(itercon_dc.keys(), 'no')
            if not args:
                raise ('Positional argument should in *args indicate constraint \
                      to be considered in scenario.')
            else:
                constraint = args[0]
                itercon_dc_temp[constraint] = 'yes'     
            for con, switch in itercon_dc_temp.items():
                opt.defines[str('py_'+con)] = '*' if switch == 'yes' else '""'
        else:
            for con, switch in itercon_dc.items():
                opt.defines[str('py_'+con)] = '*' if switch == 'yes' else '""'
    except:
        raise
        print('E:defineGAMSOptions')

def scen_solve(workdir, cp_file, sysdir, scen_run, manycsv=True, bigcsv=False, constraint=None):
    '''
    workdir: parent directory where python is excuted.
    cp_file
    '''
    ws = GamsWorkspace(system_directory=sysdir, debug=DebugLevel.KeepFiles)
    print('worker dir: ', ws.working_directory)
    cp = ws.add_checkpoint(cp_file)
    job = ws.add_job_from_string(''.join([k + " = " + str(v) + "; " for k, v in scen_run.items()]) + "\n solve dieterpy_reduced using lp min Z;\n ms=dieterpy_reduced.modelstat;\n ss=dieterpy_reduced.solvestat;", cp)
    opt = GamsOptions(ws)
    opt.all_model_types = "cplex"
    opt.optfile = 1
    opt.optdir = workdir
    # compile
    job.run(opt, output=sys.stdout)
    os.makedirs(os.path.join(workdir, "gdxout"), exist_ok=True)
    now = datetime.now()
    strnow = now.strftime("-%Y%m%d%H%M%S")
    if constraint is None:
       outgdxpath = os.path.join(workdir, 'gdxout', '_'.join([k + "_" + str(v) for k, v in scen_run.items()]) + "+ms_" + str(job.out_db["ms"].find_record().value) + "_Obj_MM_" + str(round(job.out_db["Z"].find_record().level/1000000, 1)) + "_output" + strnow + ".gdx") 
    else:
        outgdxpath = os.path.join(workdir, 'gdxout', '_'.join([k + "_" + str(v) for k, v in scen_run.items()]) + '_' + constraint + "+ms_" + str(job.out_db["ms"].find_record().value) + "_Obj_MM_" + str(round(job.out_db["Z"].find_record().level/1000000, 1)) + "_output" + strnow + ".gdx")
    job.out_db.export(outgdxpath)
    time.sleep(1)
    if manycsv:
        to_csv(outgdxpath)
    if bigcsv:
        to_bigcsv(outgdxpath, os.path.join(workdir, "csv"), ['map', 'headers'], ['data', 'upload'])
        
def runScenarios(convar_dc, iterparam_dc, ws, cp_file, sysdir, symcsv, bigcsv, *args):
    '''
    TODO

    Parameters
    ----------
    convar_dc : TYPE
        DESCRIPTION.
    iterparam_dc : TYPE
        DESCRIPTION.
    ws : TYPE
        DESCRIPTION.
    cp_file : TYPE
        DESCRIPTION.
    sysdir : TYPE
        DESCRIPTION.
    symcsv : TYPE
        DESCRIPTION.
    bigcsv : TYPE
        DESCRIPTION.
    *args : TYPE
        DESCRIPTION.

    Returns
    -------
    start_time : TYPE
        DESCRIPTION.

    '''
    try:
        wd = ws.working_directory
        if args:
            constraint = args[0]
        # conditions to be tested
        iter_para_con_test = [convar_dc['iteration_parameter'] == 'yes',
                              convar_dc['iteration_constraints'] == 'yes']
        iter_para_test = [convar_dc['iteration_parameter'] == 'yes',
                          convar_dc['iteration_constraints'] == 'no']
        iter_con_test = [convar_dc['iteration_parameter'] == 'no',
                         convar_dc['iteration_constraints'] == 'yes']
        
        if all(iter_para_con_test):
            if convar_dc['iteration_parameter_mode'] == 'matrix':
                queue = paramScenarioMatrixQueue(iterparam_dc)    
            elif convar_dc['iteration_parameter_mode'] == 'tuple':
                queue = paramScenarioTupleQueue(iterparam_dc)
            start_time = time.time()
            for scen_run in queue:
                print(':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                print('New Iteration: scenario parameter {}, constraint {}.'.format(scen_run, constraint))
                print(':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                scen_solve(wd, cp_file, sysdir, scen_run, symcsv, bigcsv, constraint)
        elif all(iter_con_test):
            start_time = time.time()
            scen_run = {}
            print(':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            print('New Iteration: constraint {}.'.format(constraint))
            print(':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            scen_solve(wd, cp_file, sysdir, scen_run, symcsv, bigcsv, constraint)
        elif all(iter_para_test):
            if convar_dc['iteration_parameter_mode'] == 'matrix':
                queue = paramScenarioMatrixQueue(iterparam_dc)    
            elif convar_dc['iteration_parameter_mode'] == 'tuple':
                queue = paramScenarioTupleQueue(iterparam_dc)
            start_time = time.time()
            for scen_run in queue:
                print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                print('New Iteration: scenario parameter {}.'.format(scen_run))
                print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                scen_solve(ws.working_directory, cp_file, sysdir, scen_run, symcsv, bigcsv)
        elif not all(iter_para_con_test):
            start_time = time.time()
            scen_run = {}
            print(':::::::::::::::::::::::::::::::')
            print('Single run starting...')
            print(':::::::::::::::::::::::::::::::')
            scen_solve(ws.working_directory, cp_file, sysdir, scen_run, symcsv, bigcsv)
        return start_time
    except:
        raise
        print('E:runScenarios')
    
        
#%%

if __name__ == '__main__':

    # create location paths for input and output
    input_path = '../input'
    gdx_path = '../gdxin'
    
    # import control and scenario variables from config file
    convar_dc = getConfigVariables(input_path, sheet_name='control_variables')
    scen_dc = getConfigVariables(input_path, sheet_name='scenario')   

    # generate input gdx files s.t. control variable "skip_import"
    generateInputGDX(input_path, gdx_path, convar_dc)
    
    # generate iterables for loop calculations
    iterparam_dc = getIteratableParameters(convar_dc, input_path, sheet_name='iteration_parameter')
    itercon_dc = getIteratableConstraints(convar_dc, input_path, sheet_name='iteration_constraints')
        
    # global option 'generate_tables_csv': generate XXX csv
    if convar_dc['generate_tables_csv'] == 'yes':
        symcsv = True
    else:
        symcsv = False
    
    # global option 'generate_tables_csv': generate one csv holding all 
    # scenario results
    if convar_dc['generate_main_csv'] == 'yes':
        bigcsv = True
    else:
        bigcsv = False

    # loop over contraints iterables
    if convar_dc['iteration_constraints'] == 'yes':    
        for constraint, switch in itercon_dc.items():
            if switch == 'yes':
                
                # prepare GAMS API
                ws, sysdir, cp, opt = prepareGAMSAPI()
                
                # handover scenario options to GAMS
                defineGAMSOptions(scen_dc, opt, itercon_dc, convar_dc, constraint)
                
                # Create model instance         
                jobs = ws.add_job_from_file(os.path.join(ws.working_directory, 'model', 'model.gms'))
                jobs.run(gams_options=opt, checkpoint=cp)
                cp_file = os.path.join(ws.working_directory, cp.name) + ".g00"
        
                # start scenario runs for parameter variation:
                start_time = runScenarios(convar_dc, iterparam_dc, ws, cp_file, sysdir, symcsv, bigcsv, constraint)
            else: 
                continue
            
    # no constraint iteration       
    else:
        # prepare GAMS API
        ws, sysdir, cp, opt = prepareGAMSAPI()
        
        # handover scenario options to GAMS
        defineGAMSOptions(scen_dc, opt, itercon_dc, convar_dc)
        
        # Create model instance
        jobs = ws.add_job_from_file(os.path.join(ws.working_directory, 'model', 'model.gms'))
        # TODO: exception does not work
        try:
            jobs.run(gams_options=opt, checkpoint=cp)
        except Exception as e:
            e = sys.exc_info()[0]
            print('Error: ', e, 'occured.')
            print('''Hint: If you loop over multiple constraints, check 
                  whether iteration_constraints are switched to "yes" on sheet
                  control_variables in config.xlsx''')
            raise
                
        cp_file = os.path.join(ws.working_directory, cp.name) + ".g00"
    
        # start scenario runs for parameter variation:
        start_time = runScenarios(convar_dc, iterparam_dc, ws, cp_file, sysdir, symcsv, bigcsv)
        
    time.sleep(2)
    print("--- %s seconds ---" % (round(time.time() - start_time - 2.0, 2)))
    print('Gams done!')
    
    rep_read = os.path.join(ws.working_directory,'csv')
    rep_dest = os.path.join(ws.working_directory, 'report')
    
    if bigcsv:
        if convar_dc['report_query']:
            query = convar_dc['report_query']
            print('Generating reports...')
            reports(query, rep_read, rep_dest)
    print('finished')

